-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : Dim 21 fév. 2021 à 18:27
-- Version du serveur :  8.0.23-0ubuntu0.20.04.1
-- Version de PHP : 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `archi_n_tiers`
--

-- --------------------------------------------------------

--
-- Structure de la table `accounts`
--

CREATE TABLE `accounts` (
  `id` int NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `authorization` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `accounts`
--

INSERT INTO `accounts` (`id`, `firstname`, `lastname`, `email`, `password`, `role`, `authorization`, `created_date`) VALUES
(1, 'Admin', 'Admin', 'admin@tse-voyages.fr', '$2y$10$haSlmvRPPOe/R/iUYc3jyer5tVnalJfy.eUlfmTCjcKlo2C5vrbue', 'Admin', 'Activated', '2021-02-21 18:06:12'),
(2, 'Alexandre', 'Solane', 'alexandresolane.web@gmail.com', '$2y$10$t7i/j8ci0N0kTIlW.gp3YOIBP2CKZLiB9WfNW2Ps49xIEq9yCmma6', 'FISE3', '8310e4ca827c733c52b2922942c89155', '2021-02-21 18:10:16');

-- --------------------------------------------------------

--
-- Structure de la table `connections`
--

CREATE TABLE `connections` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `token` varchar(255) NOT NULL,
  `time_expiration` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `connections`
--

INSERT INTO `connections` (`id`, `user_id`, `token`, `time_expiration`) VALUES
(1, 1, 'eb3a6303a05ddb34f63d6245620f1d76', '2021-02-23 18:06:59'),
(2, 2, 'c98c0bb9574552ad48349cca021105b9', '2021-02-23 18:10:40'),
(3, 1, '79c9c0ace8eaa9b3a1d3e8e050972067', '2021-02-23 18:22:35');

-- --------------------------------------------------------

--
-- Structure de la table `reset`
--

CREATE TABLE `reset` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `token` varchar(255) NOT NULL,
  `requested_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `travels`
--

CREATE TABLE `travels` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `date_begin` date NOT NULL,
  `date_end` date NOT NULL,
  `location` json NOT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `travels`
--

INSERT INTO `travels` (`id`, `user_id`, `date_begin`, `date_end`, `location`, `status`) VALUES
(2, 2, '2021-12-24', '2021-02-26', '{\"name\": \"Rio de Janeiro\", \"type\": \"locality\", \"label\": \"Rio de Janeiro, Brazil\", \"county\": \"Rio de Janeiro\", \"number\": null, \"region\": \"Rio De Janeiro\", \"street\": null, \"country\": \"Brazil\", \"map_url\": \"https://map.positionstack.com/export/embed.html?bbox=-43.796539,-23.082893,-43.099039,-22.74602&layer=mapnik&marker=-22.935024,-43.518246\", \"latitude\": -22.935024, \"locality\": \"Rio de Janeiro\", \"continent\": \"South America\", \"longitude\": -43.518246, \"confidence\": 1, \"postal_code\": null, \"region_code\": \"RJ\", \"country_code\": \"BRA\", \"neighbourhood\": null, \"administrative_area\": null}', 'En attente'),
(3, 2, '2021-02-22', '2021-08-20', '{\"name\": \"Paris\", \"type\": \"locality\", \"label\": \"Paris, France\", \"county\": null, \"number\": null, \"region\": \"Paris\", \"street\": null, \"country\": \"France\", \"latitude\": 48.858705, \"locality\": \"Paris\", \"continent\": \"Europe\", \"longitude\": 2.342865, \"confidence\": 1, \"postal_code\": null, \"region_code\": \"VP\", \"country_code\": \"FRA\", \"neighbourhood\": null, \"administrative_area\": \"Paris\"}', 'En attente'),
(4, 2, '2021-02-11', '2021-02-17', '{\"name\": \"Dakar\", \"type\": \"locality\", \"label\": \"Dakar, Senegal\", \"county\": null, \"number\": null, \"region\": \"Dakar\", \"street\": null, \"country\": \"Senegal\", \"latitude\": 14.713018, \"locality\": \"Dakar\", \"continent\": \"Africa\", \"longitude\": -17.454726, \"confidence\": 1, \"postal_code\": null, \"region_code\": \"DK\", \"country_code\": \"SEN\", \"neighbourhood\": null, \"administrative_area\": null}', 'En attente'),
(5, 2, '2022-07-21', '2023-11-21', '{\"name\": \"Sydney\", \"type\": \"locality\", \"label\": \"Sydney, NSW, Australia\", \"county\": \"Sydney\", \"number\": null, \"region\": \"New South Wales\", \"street\": null, \"country\": \"Australia\", \"latitude\": -33.868633, \"locality\": \"Sydney\", \"continent\": \"Oceania\", \"longitude\": 151.209421, \"confidence\": 1, \"postal_code\": null, \"region_code\": \"NSW\", \"country_code\": \"AUS\", \"neighbourhood\": null, \"administrative_area\": \"Sydney\"}', 'En attente');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `connections`
--
ALTER TABLE `connections`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reset`
--
ALTER TABLE `reset`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `travels`
--
ALTER TABLE `travels`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `connections`
--
ALTER TABLE `connections`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `reset`
--
ALTER TABLE `reset`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `travels`
--
ALTER TABLE `travels`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
