/**
 * Method to display activation link form instead of login form
 */
function displayNewRegistrationMailForm() {
    let formContainer = CE('div');
    formContainer.setAttribute('id','form-new-link-container');

    let backButton = CE('p');
    backButton.setAttribute('class','crud-button');
    backButton.setAttribute('id','back-verify-button');
    backButton.setAttribute('title',"Cliquez-ici pour revenir en arrière");
    backButton.setAttribute('onclick','backToPreviousForm()');
    backButton.innerHTML = "Revenir en arrière";

    let emailField = CE('input');
    emailField.setAttribute('type','email');
    emailField.setAttribute('name', 'email')
    emailField.setAttribute('id', 'email-activation');
    emailField.setAttribute('class','form-input');
    emailField.setAttribute('title','Veuillez saisir votre e-mail ici');
    emailField.setAttribute('placeholder','Votre adresse e-mail');
    emailField.setAttribute('required','');

    let sendNewRegistrationMail = CE('p');
    sendNewRegistrationMail.setAttribute('class','crud-button');
    sendNewRegistrationMail.setAttribute('id','send-activation-link-button');
    sendNewRegistrationMail.setAttribute('title',"Cliquez-ici pour renvoyer un lien d'activation");
    sendNewRegistrationMail.setAttribute('onclick','sendNewRegistrationMail()');
    sendNewRegistrationMail.innerHTML = "Renvoyer le lien d'activation";

    let messageToUser = CE('p');
    messageToUser.setAttribute('id','message-to-user');

    formContainer.appendChild(backButton);
    formContainer.appendChild(messageToUser);
    formContainer.appendChild(emailField);
    formContainer.appendChild(sendNewRegistrationMail);

    if (page === "Verify.php") {
        _('activate-button-container').remove();
        _('div-verify-container').appendChild(formContainer);
    } else if (page === "Login.php") {
        $('#login-form').empty();
        _('login-form').appendChild(formContainer);
    }
}

/**
 * Method to back on login page for differents view
 */
function backToPreviousForm() {
    if (page === 'Login.php') {
        window.location.href = window.location.href;
    } else if (page === 'Verify.php') {
        let activateButtonContainer = CE('div');
        activateButtonContainer.setAttribute('id','activate-button-container');

        let activationLinkButton = CE('p');
        activationLinkButton.setAttribute('class','crud-button');
        activationLinkButton.setAttribute('id','activation-link-button');
        activationLinkButton.setAttribute('title',"Cliquez-ici pour renvoyer un lien d'activation");
        activationLinkButton.setAttribute('onclick','displayNewRegistrationMailForm()');
        activationLinkButton.innerHTML = "Renvoyer le lien d'activation";

        activateButtonContainer.appendChild(activationLinkButton);

        _('form-new-link-container').remove();
        _('div-verify-container').appendChild(activateButtonContainer);
    }
}

/**
 * Method to send a new activation mail by AJAX calling
 */
function sendNewRegistrationMail() {
    // Clean style
    _("email-activation").removeAttribute("style");

    // Field storage
    let email = _("email-activation").value;

    $.ajax({
        url: '../../api/ajax/Register.php',
        type: 'POST',
        data: { action: 'sendNewMail', 
                email: email
        },
        success: function (data) {
            if (data['status'] === 'success') {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#2fb541';
                _("message-to-user").innerHTML = 'Un nouveau mail de validation vient de vous être envoyé';
            } else if (data['message'] === "Mail couldd not be send") {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Le mail n\'a pas pu être envoyé. Veuillez réessayez ultérieurement';
            } else if (data['message'] === "Mail was not found") {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = "Aucun compte avec cette adresse e-mail n'a été trouvé";
            } else if (data['message'] === 'Account is already activated') {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#2fb541';
                _("message-to-user").innerHTML = 'Votre compte est déjà activé';
            } else {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Veuillez compléter les champs vides';
                if (email === "") {
                    _("email-activation").style.borderColor = '#ff2e1f';
                }
            }
            // console.log(JSON.stringify(data, null, 2));
        },
        error: function (data) {
            _("message-to-user").style.display = 'block';
            _("message-to-user").style.color = '#ff2e1f';
            _("message-to-user").innerHTML = 'Une erreur est survenue';
            // console.log(JSON.stringify(data, null, 2));
        }
    });
}