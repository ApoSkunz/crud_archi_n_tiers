// Const for regex match on diferent mail type
const regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

/**
 * AutoCall function on loading page to add Listenner on input elements
 */
(function () {
    if (_("password-log")) {
        _("password-log").addEventListener('input', formIsNotEmpty);
        _("email-log").addEventListener('input', formIsNotEmpty);
    }
}) ();

/**
 * Method to check if the form is empty and display or not the sign in button
 */
function formIsNotEmpty() {
    if (_("password-log").value !== "" && _("email-log").value !== "" && _("email-log").value.match(regexEmail)) {
        _("login-button-container").style.display = "block";
        _("login-button-container").style.animation = "opacity-transition 1s linear 1";
    } else {
        _("login-button-container").removeAttribute("style");
    }
}

/**
 * Method to sign in by AJAX Calling on user credentials
 */
function toLogIn() {

    // Clean style
    _("email-log").removeAttribute("style");
    _("password-log").removeAttribute("style");
    _("message-to-user").removeAttribute("style");

    // Field storage
    let email = _("email-log").value;
    let password = _("password-log").value;

    $.ajax({
        url: '../../api/ajax/Login.php',
        type: 'POST',
        data: { action: 'toLogIn', 
                email: email,
                password: password
        },
        success: function (data) {
            if (data['status'] === 'success') {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#2fb541';
                _("message-to-user").innerHTML = 'Connexion en cours';
        
                if (data['role'] === 'Admin') {
                    setTimeout(function() { 
                        document.location.href = "Admin-Dashboard.php";
                    }, 1750); 
                } else {
                    setTimeout(function() { 
                        document.location.href = "Dashboard.php";
                    }, 1750); 
                }
            } else if (data['message'] === "Email is not valid") {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Veuillez saisir un email valide';
            } else if (data['message'] === "Information are incorrect") {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Les identifiants ne correspondent pas';
            } else {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Veuillez compléter les champs vides';
                if (email === "") {
                    _("email-log").style.borderColor = '#ff2e1f';
                } 
                if (password === "") {
                    _("password-log").style.borderColor = '#ff2e1f';
                }
            }
            // console.log(JSON.stringify(data, null, 2));
        },
        error: function (data) {
            _("message-to-user").style.display = 'block';
            _("message-to-user").style.color = '#ff2e1f';
            _("message-to-user").innerHTML = 'Une erreur est survenue';
            // console.log(JSON.stringify(data, null, 2));
        }
    });

}

/**
 * AJAX Calling to signOut user
 * @display a response on PHP AJAX Data
 */
function signOut() {
    $.ajax({
        url: '../../api/ajax/Login.php',
        type: 'POST',
        data: {action: 'signOut'},
        success: function (data) {
            if (data['status'] === 'success') {
                // window.alert('Vous allez être déconnecté');
                // setTimeout(function() { 
                //     document.location.href = window.location.href;
                // }, 1750);
                document.location.href = window.location.href;
            } else if (data['message'] === "JWT is expired") {
                window.alert('Votre jeton de connexion a expiré');
                setTimeout(function() { 
                    document.location.href = window.location.href;
                }, 1750);
            } else if (data['message'] === "JWT can only be used between nbf and iat") {
                window.alert("Vous appartenez au passé ou au futur");
                setTimeout(function() { 
                    document.location.href = window.location.href;
                }, 1750);
            } else if (data['message'] === "Signature is not correct") {
                window.alert('Vous allez être déconnecté');
                setTimeout(function() { 
                    document.location.href = window.location.href;
                }, 1750);
            } else {
                window.alert('Vous devez être connecté pour vous déconnecter');
            }
            // console.log(JSON.stringify(data, null, 2));
        },
        error: function (data) {
            window.alert('Une erreur est survenue');
            // console.log(JSON.stringify(data, null, 2));
        }
    });
}

function formSendNewRegistrationMail() {
    let formContainer = document.createElement('div');
    formContainer.setAttribute('id','form-new-link-container');

    let backButton = document.createElement('p');
    backButton.setAttribute('class','crud-button');
    backButton.setAttribute('id','back-verify-button');
    backButton.setAttribute('title',"Cliquez-ici pour rerevenir en arrière");
    backButton.setAttribute('onclick','backVerify()');
    backButton.innerHTML = "Revenir en arrière";

    let emailField = document.createElement('input');
    emailField.setAttribute('type','email');
    emailField.setAttribute('name', 'email')
    emailField.setAttribute('id', 'email-activation');
    emailField.setAttribute('class','form-input');
    emailField.setAttribute('title','Veuillez saisir votre e-mail ici');
    emailField.setAttribute('placeholder','Votre adresse e-mail');
    emailField.setAttribute('required','');

    let sendNewRegistrationMail = document.createElement('p');
    sendNewRegistrationMail.setAttribute('class','crud-button');
    sendNewRegistrationMail.setAttribute('id','send-activation-link-button');
    sendNewRegistrationMail.setAttribute('title',"Cliquez-ici pour renvoyer un lien d'activation");
    sendNewRegistrationMail.setAttribute('onclick','sendNewRegistrationMail()');
    sendNewRegistrationMail.innerHTML = "Renvoyer le lien d'activation";

    let messageToUser = document.createElement('p');
    messageToUser.setAttribute('id','message-to-user');

    formContainer.appendChild(backButton);
    formContainer.appendChild(messageToUser);
    formContainer.appendChild(emailField);
    formContainer.appendChild(sendNewRegistrationMail);

    _('activate-button-container').remove();
    _('div-verify-container').appendChild(formContainer);
}

function backVerify() {
    let activateButtonContainer = document.createElement('div');
    activateButtonContainer.setAttribute('id','activate-button-container');

    let activationLinkButton = document.createElement('p');
    activationLinkButton.setAttribute('class','crud-button');
    activationLinkButton.setAttribute('id','activation-link-button');
    activationLinkButton.setAttribute('title',"Cliquez-ici pour renvoyer un lien d'activation");
    activationLinkButton.setAttribute('onclick','formSendNewRegistrationMail()');
    activationLinkButton.innerHTML = "Renvoyer le lien d'activation";

    activateButtonContainer.appendChild(activationLinkButton);

    _('form-new-link-container').remove();
    _('div-verify-container').appendChild(activateButtonContainer);
}