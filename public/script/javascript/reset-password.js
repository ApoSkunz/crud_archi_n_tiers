/**
 * Method to display reset password form instead of login form
 */
function displayResetPasswordForm() {
    let formContainer = CE('div');
    formContainer.setAttribute('id','form-new-link-container');

    let backButton = CE('p');
    backButton.setAttribute('class','crud-button');
    backButton.setAttribute('id','back-verify-button');
    backButton.setAttribute('title',"Cliquez-ici pour revenir en arrière");
    backButton.setAttribute('onclick','backToPreviousForm()');
    backButton.innerHTML = "Revenir en arrière";

    let emailField = CE('input');
    emailField.setAttribute('type','email');
    emailField.setAttribute('name', 'email')
    emailField.setAttribute('id', 'email-reset');
    emailField.setAttribute('class','form-input');
    emailField.setAttribute('title','Veuillez saisir votre e-mail ici');
    emailField.setAttribute('placeholder','Votre adresse e-mail');
    emailField.setAttribute('required','');

    let sendNewRegistrationMail = CE('p');
    sendNewRegistrationMail.setAttribute('class','crud-button');
    sendNewRegistrationMail.setAttribute('id','reset-pwd-button');
    sendNewRegistrationMail.setAttribute('title',"Cliquez-ici pour réinitialiser votre mot de passe");
    sendNewRegistrationMail.setAttribute('onclick','sendResetPasswordMail()');
    sendNewRegistrationMail.innerHTML = "Réinitialiser mon mot de passe";

    let messageToUser = CE('p');
    messageToUser.setAttribute('id','message-to-user');

    formContainer.appendChild(backButton);
    formContainer.appendChild(messageToUser);
    formContainer.appendChild(emailField);
    formContainer.appendChild(sendNewRegistrationMail);

    $('#login-form').empty();
    _('login-form').appendChild(formContainer);
}

/**
 * Method to send mail to reset password by AJAX calling
 */
function sendResetPasswordMail() {
    // Clean style
    _("email-reset").removeAttribute("style");

    // Field storage
    let email = _("email-reset").value;

    $.ajax({
        url: '../../api/ajax/Account.php',
        type: 'POST',
        data: { action: 'sendResetPasswordMail', 
                email: email
        },
        success: function (data) {
            if (data['status'] === 'success') {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#2fb541';
                _("message-to-user").innerHTML = 'Un nouveau mail de validation vient de vous être envoyé';
            } else if (data['message'] === "Mail couldd not be send") {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Le mail n\'a pas pu être envoyé. Veuillez réessayez ultérieurement';
            } else if (data['message'] === "Mail was not found") {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = "Aucun compte avec cette adresse e-mail n'a été trouvé";
            } else if (data['message'] === 'Account is already activated') {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#2fb541';
                _("message-to-user").innerHTML = 'Votre compte est déjà activé';
            } else {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Veuillez compléter les champs vides';
                if (email === "") {
                    _("email-activation").style.borderColor = '#ff2e1f';
                }
            }
            // console.log(JSON.stringify(data, null, 2));
        },
        error: function (data) {
            _("message-to-user").style.display = 'block';
            _("message-to-user").style.color = '#ff2e1f';
            _("message-to-user").innerHTML = 'Une erreur est survenue';
            // console.log(JSON.stringify(data, null, 2));
        }
    });
}

/**
 * Method to reset password by AJAX calling
 */
function resetPassword() {
    let password = _("password").value;

    $.ajax({
        url: '../../api/ajax/Account.php',
        type: 'POST',
        data: { action: 'resetPassword', 
                password: password,
                userId : userId
        },
        success: function (data) {
            if (data['status'] === 'success') {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#2fb541';
                _("message-to-user").innerHTML = 'Votre mot de passe vient d\'être changé';
            } else if (data['message'] === 'Server error') {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Une erreur serveur est survenue';
            } else {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Veuillez compléter les champs vides';
                _("password").style.borderColor = '#ff2e1f';
            }
            // console.log(JSON.stringify(data, null, 2));
        },
        error: function (data) {
            _("message-to-user").style.display = 'block';
            _("message-to-user").style.color = '#ff2e1f';
            _("message-to-user").innerHTML = 'Une erreur est survenue';
            // console.log(JSON.stringify(data, null, 2));
        }
    });
}