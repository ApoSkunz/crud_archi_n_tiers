/**
 * Method to register new account by AJAX calling on different fields
 */
function toRegister() {

    // Clean style
    _("email").removeAttribute("style");
    _("password").removeAttribute("style");
    _("confirmation-password").removeAttribute("style");
    _("firstname").removeAttribute("style");
    _("lastname").removeAttribute("style");
    _("promotion").removeAttribute("style");
    _("message-to-user").removeAttribute("style");

    // Field storage
    let email = _("email").value;
    let password = _("password").value;
    let passwordConfirmation = _("confirmation-password").value;
    let firstname = _("firstname").value;
    let lastname = _("lastname").value;
    let promotion = _("promotion").value;

    $.ajax({
        url: '../../api/ajax/Register.php',
        type: 'POST',
        data: { action: 'registerNewAccount', 
                email: email,
                password: password,
                passwordConfirmation: passwordConfirmation,
                firstname: firstname,
                lastname: lastname,
                role: promotion
        },
        success: function (data) {
            if (data['status'] === 'success') {
                if (data['message'] === "New user has been registered") {
                    _("message-to-user").style.display = 'block';
                    _("message-to-user").style.color = '#2fb541';
                    _("message-to-user").innerHTML = 'Un mail de validation vient de vous être envoyé';
                } else {
                    _("message-to-user").style.display = 'block';
                    _("message-to-user").style.color = '#ffff00';
                    _("message-to-user").innerHTML = 'Le mail de validation n\'a pas pu être envoyé';
                }
            } else if (data['message'] === "Email is not valid") {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Le mail n\'est pas valide';
            } else if (data['message'] === "Email already exists") {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Un utilisateur avec cet email existe déjà';
            } else if (data['message'] === "Passwords don't match") {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Les mots de passe ne correspondent pas';
            } else if (data['message'] === "Role is not authorized") {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'La promotion n\'est pas correcte';
            } else if (data['message'] === "An error has occured") {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Une erreur du serveur est intervenue';
            } else {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Veuillez compléter les champs vides';
                if (email === "") {
                    _("email").style.borderColor = '#ff2e1f';
                } if (password === "") {
                    _("password").style.borderColor = '#ff2e1f';
                } if (passwordConfirmation === "") {
                    _("confirmation-password").style.borderColor = '#ff2e1f';
                } if (firstname === "") {
                    _("firstname").style.borderColor = '#ff2e1f';
                } if (lastname === "") {
                    _("lastname").style.borderColor = '#ff2e1f';
                } if (promotion === "") {
                    _("promotion").style.borderColor = '#ff2e1f';
                }
            }
            // console.log(JSON.stringify(data, null, 2));
        },
        error: function (data) {
            _("message-to-user").style.display = 'block';
            _("message-to-user").style.color = '#ff2e1f';
            _("message-to-user").innerHTML = 'Une erreur est survenue';
            // console.log(JSON.stringify(data, null, 2));
        }
    });
}