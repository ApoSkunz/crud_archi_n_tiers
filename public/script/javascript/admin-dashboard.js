
/**
 * Method to search students mobilities by AJAX calling on multi criterions
 * And change results in table and on map
 */
function searchStudents() {
  _("message-to-user").removeAttribute("style");
  _("message-to-user").style.textAlign = "center";

  let student = _('student').value;
  let country = _('country').value;
  let promotion = _('promotion').value;
  let end_date = _('end_date').value;
  let begin_date = _('begin_date').value;

  $.ajax({
    url: '../../api/ajax/Travel.php',
    type: 'POST',
    data: { action: 'searchStudents', 
            student: student,
            country: country,
            promotion: promotion,
            end_date: end_date,
            begin_date: begin_date
    },
    success: function (data) {
        if (data['status'] === 'success') {
            let travels = data["data"];
            
            if (travels.length == 0) {
              _("message-to-user").style.display = 'block';
              _("message-to-user").style.color = '#242424';
              _("message-to-user").innerHTML = 'Aucun résultat n\'a été trouvé';
            }
            $('#tbody-admin').empty();
            $('#chartdiv').empty();
            for (let j=0; j < travels.length; j++) {
              let tr = document.createElement('tr');
              let td = document.createElement('td');
              td.innerHTML = travels[j][0];
              tr.appendChild(td);
              let td1 = document.createElement('td');
              td1.innerHTML = travels[j][1];
              tr.appendChild(td1);
              let td2 = document.createElement('td');
              td2.innerHTML = travels[j][2];
              tr.appendChild(td2);
              let td3 = document.createElement('td');
              td3.innerHTML = travels[j][3];
              tr.appendChild(td3);
              let td4 = document.createElement('td');
              td4.innerHTML = travels[j][4];
              tr.appendChild(td4);
              let td5 = document.createElement('td');
              td5.innerHTML = travels[j][5];
              tr.appendChild(td5);
              _('tbody-admin').appendChild(tr);
            }

            am4core.ready(function() {

              // Themes begin
              am4core.useTheme(am4themes_animated);
              // Themes end
          
              // Create map instance
              var chart = am4core.create("chartdiv", am4maps.MapChart);
          
              // Set map definition
              chart.geodata = am4geodata_worldLow;
          
              // Set projection
              chart.projection = new am4maps.projections.Miller();
          
              // Create map polygon series
              var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
          
              // Exclude Antartica
              polygonSeries.exclude = ["AQ"];
          
              // Make map load polygon (like country names) data from GeoJSON
              polygonSeries.useGeodata = true;
          
              // Configure series
              var polygonTemplate = polygonSeries.mapPolygons.template;
              polygonTemplate.tooltipText = "{name}";
              polygonTemplate.polygon.fillOpacity = 0.8;
              polygonTemplate.polygon.fill = "#fcf8ed";
          
          
              // Create hover state and set alternative fill color
              var hs = polygonTemplate.states.create("hover");
              hs.properties.fill = chart.colors.getIndex(0);
          
              // Add image series
              var imageSeries = chart.series.push(new am4maps.MapImageSeries());
              imageSeries.mapImages.template.propertyFields.longitude = "longitude";
              imageSeries.mapImages.template.propertyFields.latitude = "latitude";
              imageSeries.mapImages.template.tooltipText = "Ville : {title}, Étudiant : {studentName}";
              imageSeries.mapImages.template.propertyFields.url = "url";
          
              var circle = imageSeries.mapImages.template.createChild(am4core.Circle);
              circle.radius = 6;
              circle.propertyFields.fill = "color";
          
              var circle2 = imageSeries.mapImages.template.createChild(am4core.Circle);
              circle2.radius = 6;
              circle2.propertyFields.fill = "color";
          
          
              circle2.events.on("inited", function(event){
              animateBullet(event.target);
              })
          
          
              function animateBullet(circle) {
                  var animation = circle.animate([{ property: "scale", from: 1, to: 5 }, { property: "opacity", from: 1, to: 0 }], 1000, am4core.ease.circleOut);
                  animation.events.on("animationended", function(event){
                  animateBullet(event.target.object);
                  })
              }
          
              var colorSet = new am4core.ColorSet();
              imageSeries.data = [];
              for (let j=0; j < travels.length; j++) {
                let json = {}
                json.title = travels[j][2];
                json.latitude = travels[j][6];
                json.longitude = travels[j][7];
                json.color = colorSet.next();
                json.studentName = travels[j][8];
                imageSeries.data[j] = json;
              }
              
              });
        } else if (data['message'] === "Server error") {
            _("message-to-user").style.display = 'block';
            _("message-to-user").style.color = '#ff2e1f';
            _("message-to-user").innerHTML = 'Une erreur serveur est survenue';
        } else {
            _("message-to-user").style.display = 'block';
            _("message-to-user").style.color = '#ff2e1f';
            _("message-to-user").innerHTML = 'Veuillez saisir au moins un champs de la barre de recherche';
        }
        // console.log(JSON.stringify(data, null, 2));
    },
    error: function (data) {
        _("message-to-user").style.display = 'block';
        _("message-to-user").style.color = '#ff2e1f';
        _("message-to-user").innerHTML = 'Une erreur est survenue';
        // console.log(JSON.stringify(data, null, 2));
    }
  });
}

/**
 * Method to validate a mobility by AJAX calling
 * @param {String} travelId represents the id of the travel to validate 
 */
function updateStatusOn(travelId) {
  _("message-to-user").removeAttribute("style");
  _("message-to-user").style.textAlign = "center";

  $.ajax({
    url: '../../api/ajax/Travel.php',
    type: 'POST',
    data: { action: 'updateTravelStatus', 
            travelId: travelId
    },
    success: function (data) {
        if (data['status'] === 'success') {
            _("message-to-user").style.display = 'block';
            _("message-to-user").style.color = '#2fb541';
            _("message-to-user").innerHTML = 'Statut mis à jour';
            _('travel-'+data["travelId"]).innerHTML = 'Validé';
        } else if (data['message'] === "Server error") {
            _("message-to-user").style.display = 'block';
            _("message-to-user").style.color = '#ff2e1f';
            _("message-to-user").innerHTML = 'Une erreur serveur est survenue';
        } else {
            _("message-to-user").style.display = 'block';
            _("message-to-user").style.color = '#ff2e1f';
            _("message-to-user").innerHTML = 'Veuillez saisir au moins un champs de la barre de recherche';
        }
        // console.log(JSON.stringify(data, null, 2));
    },
    error: function (data) {
        _("message-to-user").style.display = 'block';
        _("message-to-user").style.color = '#ff2e1f';
        _("message-to-user").innerHTML = 'Une erreur est survenue';
        // console.log(JSON.stringify(data, null, 2));
    }
  });
}