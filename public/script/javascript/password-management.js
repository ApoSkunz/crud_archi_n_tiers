/**
 * Method to mask or unmask password
 * @param {String} idInput represents the id of password field 
 * @param {String} idButton represents the id of button eye to mask or unmask password
 */
function maskOrUnmask(idInput,idButton) {
    let myFieldToChange = _(idInput);
    let myButtonToChange = _(idButton);
    
    if (myFieldToChange.type == "password") {
        myFieldToChange.type = "text";
        myButtonToChange.title = "Cliquez-ici pour masquer le mot de passe";
        myButtonToChange.className = "fas fa-eye";
    } 
    else {
        myFieldToChange.type = "password";
        myButtonToChange.title = "Cliquez-ici pour démasquer le mot de passe";
        myButtonToChange.className = "fas fa-eye-slash";
    }
}