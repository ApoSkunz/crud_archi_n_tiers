/**
 * Method to return element by id
 * @param {String} id represents id of an element
 */
function _(id) {
    return document.getElementById(id);
}

/**
 * Method to return List of elements by class name
 * @param {String} className represents class name of an element
 */
function _C(className) {
    return document.getElementsByClassName(className);
}

/**
 * Method to crate element on tag name
 * @param {String} tagName represents tag name of an element 
 */
function CE(tagName) {
    return document.createElement(tagName);
}