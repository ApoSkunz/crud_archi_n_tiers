/**
 * Method to add a travel by AJAX
 */
function addTravel(){

    // Field storage
    let city = _("city").value;
    let date_begin = _("date-begin").value;
    let date_end = _("date-end").value;

    $.ajax({
        url: '../../api/ajax/Travel.php',
        type: 'POST',
        data: { action: 'addTravel',
                city: city,
                date_begin: date_begin,
                date_end: date_end,
                userId: userId
        },

        success: function (data) {
            if (data['status'] === 'success') {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#2fb541';
                _("message-to-user").innerHTML = 'Vous avez ajouté un voyage';
                setTimeout(function() { 
                    document.location.href = "Dashboard.php";
                }, 1750); 
                console.log(JSON.stringify(data, null, 2));
            }
            else if(data['message'] === 'Location is empty'){
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = 'ff2e1f';
                _("message-to-user").innerHTML = 'Veuillez rentrer un nom de ville valide';
            }
            else if(data['message'] === 'DateBegin cannot be > than DateEnd'){
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = 'ff2e1f';
                _("message-to-user").innerHTML = 'Veuillez rentrer une date de début inférieure à une date de fin';
            }
            else if(data['message'] === "Server error") {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Une erreur serveur est survenue';
            }
            else {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Veuillez saisir une ville/Date de début/Date de fin';
            }
        },
        error: function (data) {
            _("message-to-user").style.display = 'block';
            _("message-to-user").style.color = '#ff2e1f';
            _("message-to-user").innerHTML = 'Une erreur est survenue';
            console.log(JSON.stringify(data, null, 2));
        }
    });
}

/**
 * Method to delete a travel by AJAX calling
 * @param {String} travelId represents the id of the travel to delete
 */
function deleteTravel(travelId){

    $.ajax({
        url: '../../api/ajax/Travel.php',
        type: 'POST',
        data: { action: 'deleteTravel',
                travelId: travelId,
                userId: userId
        },

        success: function (data) {
            if (data['status'] === 'success') {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#2fb541';
                _("message-to-user").innerHTML = 'Vous avez supprimé un voyage';
                setTimeout(function() { 
                    document.location.href = "Dashboard.php";
                }, 1750); 
                console.log(JSON.stringify(data, null, 2));
            }
            else if (data['message'] === "No travels found with this id") {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'L\'id associé n\'est pas le bon !';
            }
            else if (data['message'] === "Server error") {
                _("message-to-user").style.display = 'block';
                _("message-to-user").style.color = '#ff2e1f';
                _("message-to-user").innerHTML = 'Une erreur serveur est survenue';
            }
        },
        error: function (data) {
            _("message-to-user").style.display = 'block';
            _("message-to-user").style.color = '#ff2e1f';
            _("message-to-user").innerHTML = 'Une erreur est survenue';
            console.log(JSON.stringify(data, null, 2));
        }
    });
}