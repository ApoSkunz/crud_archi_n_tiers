<section id="section-verify">
    <div id="div-verify-container">
        <?php

        if ($scanMessage === "Validated account") {

        ?>
        <div id="title-verify-container">
            <h4 id="title-verify-4">Compte validé</h4>
            <h5 id="title-verify-5">Vous pouvez à présent accéder à votre compte</h5>
            <div id="back-to-home-container">
                <a href="Login.php" title="Cliquez-ici pour retourner sur la page e connexion" id="circle-button-home"><i class="fas fa-home"></i></a>
            </div>
        </div>
        <?php

        } else if ($scanMessage === "JWT is expired") {

        ?>
        <div id="title-verify-container">
            <h4 id="title-verify-4">Compte non validé</h4>
            <h5 id="title-verify-5">Vous êtes en possession d'un token qui est expiré</h5>
            <div id="back-to-home-container">
                <a href="Login.php" title="Cliquez-ici pour retourner sur la page e connexion" id="circle-button-home"><i class="fas fa-home"></i></a>
            </div>
        </div>
        <div id="activate-button-container">
            <p class="crud-button" id="activation-link-button" title="Cliquez-ici pour renvoyer un lien d'activation" onclick="displayNewRegistrationMailForm()">Renvoyer le lien d'activation</p>
        </div>
        <?php

        } else {

        ?>
        <div id="title-verify-container">
            <h4 id="title-verify-4">Email non vérifiée</h4>
            <h5 id="title-verify-5">Vérification eronnée</h5>
        </div>
        <div id="disclaimer-verify">
            Vous êtes en possession d'un token qui n'existe pas
        </div>
        <div id="back-to-home-container">
            <a href="Login.php" title="Cliquez-ici pour retourner sur la page de connexion" id="circle-button-home"><i class="fas fa-home"></i></a>
        </div>
        <?php
                    
        }
        
        ?>
    </div>
</section>