<section id="section-verify">
    <div id="div-verify-container">
        <?php

        if ($scanMessage === "JWT is authentifcated") {

        ?>
        <div id="title-verify-container">
            <h4 id="title-verify-4">Réinitialisation de votre mot de passe</h4>
            <p id="message-to-user"></p>
            <div>
                <input type="password" name="password" id="password" title="Veuillez confirmer votre mot de passe" class="form-input register-input-credentials" placeholder="Votre mot de passe" required="">
                <i class="fas fa-eye-slash" id="unmask-1" title="Cliquez-ici pour démasquer le mot de passe" onclick="maskOrUnmask('password','unmask-1')"></i>
            </div>
            <div id="reset-button-container">
                <p id="reset-button" class="crud-button" title="Cliquez-ici pour vous changer votre mot de passe" onclick="resetPassword()">Réinitialiser le mot de passe</p>
            </div>
            <div id="back-to-home-container">
                <a href="Login.php" title="Cliquez-ici pour retourner sur la page e connexion" id="circle-button-home"><i class="fas fa-home"></i></a>
            </div>
        </div>
        <?php

        } else {

        ?>
        <div id="title-verify-container">
            <h4 id="title-verify-4">Réinitialisation impossible</h4>
            <h5 id="title-verify-5">Vous êtes en possession d'un token qui est expiré ou non valide</h5>
            <div id="back-to-home-container">
                <a href="Login.php" title="Cliquez-ici pour retourner sur la page de connexion" id="circle-button-home"><i class="fas fa-home"></i></a>
            </div>
        </div>
        <?php

        }
        
        ?>
    </div>
</section>