<h1>Dashboard Etudiant</h1>
<p id="message-to-user"></p>
    <div id="table-container-student">
        <div id="table-student-container">
            <table id="table-student">
                <caption>Voyage.s</caption>
                <thead>
                    <tr>
                        <th>Ville</th>
                        <th>Date de départ</th>
                        <th>Date de fin</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($nbrOfTravelsOnUserId === 0){
                            ?>
                                <tr>
                                    <td colspan=4><span>Aucuns voyages encore rentrés !</span></td>
                                </tr>
                            <?php
                        }
                        else{
                            while ($currentTravel = $travelsStudent->fetch()) {
                                ?>
                                    <tr>
                                        <td><?php echo json_decode($currentTravel["location"], true)["name"];?></td>
                                        <td><?php echo $currentTravel["date_begin"]?></td>
                                        <td><?php echo $currentTravel["date_end"]?></td>
                                        <td><?php echo $currentTravel["status"]?></td>
                                        <td>
                                            <div id="deleteTravel-button-container">
                                                <p id=<?php echo $currentTravel["id"]?> class="crud-button" title="Cliquez-ici pour supprimer un voyage" onclick="deleteTravel(this.id)">Supprimer un voyage</p>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                            }
                        }
                    ?>
                    <tr>
                        <td><input type="text" name="city" id="city"></td>
                        <td><input type="date" name="date-begin" id="date-begin"></td>
                        <td><input type="date" name="date-end" id="date-end"></td>
                        <td></td>
                        <td>
                            <div id="addTravel-button-container">
                                <p id="addTravel-button" class="crud-button" title="Cliquez-ici pour ajouter un voyage" onclick="addTravel()">Ajouter un voyage</p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    