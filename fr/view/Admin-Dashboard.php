<section id="admin-dashboard">
    <div id="global-container-admin">
        <div id="filter-container">
            <h3 id="t-fb">Rechercher des mobilités</h3>
                <div id="filter-bar">
                    <input type="text" id="student" name="student-lastname" placeholder="Nom de l'étudiant"/>
                    <select name="country" id="country" class="select-promotion dash-select" required>
                        <option value="" class="option-select-promotion" selected disabled>Pays</option>
                        <?php
                            for ($index=0; $index < sizeof($countries); $index++) {
                                ?>
                                    <option value="<?php echo $countries[$index]; ?>" class="option-select-promotion"><?php echo $countries[$index]; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                    <select name="promotion" id="promotion" class="select-promotion dash-select" required>
                        <option value="" class="option-select-promotion" selected disabled>Promotion</option>
                        <option value="CITISE1" class="option-select-promotion">CITISE1</option>
                        <option value="CITISE2" class="option-select-promotion">CITISE2</option>
                        <option value="DCMIN1" class="option-select-promotion">DCMIN1</option>
                        <option value="DCMIN2" class="option-select-promotion">DCMIN2</option>
                        <option value="DE1" class="option-select-promotion">DE1</option>
                        <option value="DE2" class="option-select-promotion">DE2</option>
                        <option value="DE3" class="option-select-promotion">DE3</option>
                        <option value="DTA" class="option-select-promotion">DTA</option>
                        <option value="FISE1" class="option-select-promotion">FISE1</option>
                        <option value="FISE2" class="option-select-promotion">FISE2</option>
                        <option value="FISE3" class="option-select-promotion">FISE3</option>
                        <option value="IPSI1" class="option-select-promotion">IPSI1</option>
                        <option value="IPSI2" class="option-select-promotion">IPSI2</option>
                        <option value="IPSI3" class="option-select-promotion">IPSI3</option>
                        <option value="L3" class="option-select-promotion">L3</option>
                        <option value="SMW" class="option-select-promotion">SMW</option>
                    </select>
                    <label class="date" for="begin_date">Date d'arrivée</label>
                    <input class="date-input" type="date" id="begin_date" name="trip-start">
                    <label class="date" for="end_date">Date de départ</label>
                    <input class="date-input" type="date" id="end_date" name="trip-start">
                    <button class="crud-button dash-button" title="Cliquez-ici pour effectuer une recherche" onclick="searchStudents()">Rechercher</button>
                </div>
        </div>
        <div id="table-container-admin">
            <h4>Mobilités en cours</h4>
            <div id="table-admin-container">
                <table id="table-admin">
                    <thead>
                        <tr>
                            <th>Nom de l'étudiant</th>
                            <th>Promotion</th>
                            <th>Ville</th>
                            <th>Date de d'arrivée</th>
                            <th>Date de départ</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody id="tbody-admin">
                    <?php

                        while ($currentTravel = $travelsThird->fetch()) {
                            $beginDate = date_create($currentTravel['date_begin']);
                            $endDate = date_create($currentTravel['date_end']);
                            ?>
                            <tr>
                                <td><?php echo strtoupper($currentTravel["lastname"]).' '.$currentTravel["firstname"]; ?></td>
                                <td><?php echo $currentTravel['role']; ?></td>
                                <td><?php echo json_decode($currentTravel["location"], true)["name"]; ?></td>
                                <td><?php echo date_format($beginDate,"d/m/Y"); ?></td>
                                <td><?php echo date_format($endDate,"d/m/Y"); ?></td>
                                <td class="td-status" id='travel-<?php echo $currentTravel['id']; ?>' title="Cliquez-ici pour valider la demande de mobilité" onclick="updateStatusOn('<?php echo $currentTravel['id']; ?>')"><?php echo $currentTravel['status']; ?></td>
                            </tr>
                            <?php
                        }
                    ?>
                    </tbody>
                </table>
            </div>
            <p id="message-to-user"></p>
        </div>
    </div>
    <div id="map-container">
        <h3>Carte des mobilités</h3>
        <div id="chartdiv"></div>
    </div>
</section>
<!-- Map view -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/maps.js"></script>
<script src="https://cdn.amcharts.com/lib/4/geodata/worldLow.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
<script>
    am4core.ready(function() {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create map instance
    var chart = am4core.create("chartdiv", am4maps.MapChart);

    // Set map definition
    chart.geodata = am4geodata_worldLow;

    // Set projection
    chart.projection = new am4maps.projections.Miller();

    // Create map polygon series
    var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

    // Exclude Antartica
    polygonSeries.exclude = ["AQ"];

    // Make map load polygon (like country names) data from GeoJSON
    polygonSeries.useGeodata = true;

    // Configure series
    var polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    polygonTemplate.polygon.fillOpacity = 0.8;
    polygonTemplate.polygon.fill = "#fcf8ed";


    // Create hover state and set alternative fill color
    var hs = polygonTemplate.states.create("hover");
    hs.properties.fill = chart.colors.getIndex(0);

    // Add image series
    var imageSeries = chart.series.push(new am4maps.MapImageSeries());
    imageSeries.mapImages.template.propertyFields.longitude = "longitude";
    imageSeries.mapImages.template.propertyFields.latitude = "latitude";
    imageSeries.mapImages.template.tooltipText = "Ville : {title}, Étudiant : {studentName}";
    imageSeries.mapImages.template.propertyFields.url = "url";

    var circle = imageSeries.mapImages.template.createChild(am4core.Circle);
    circle.radius = 6;
    circle.propertyFields.fill = "color";

    var circle2 = imageSeries.mapImages.template.createChild(am4core.Circle);
    circle2.radius = 6;
    circle2.propertyFields.fill = "color";


    circle2.events.on("inited", function(event){
    animateBullet(event.target);
    })


    function animateBullet(circle) {
        var animation = circle.animate([{ property: "scale", from: 1, to: 5 }, { property: "opacity", from: 1, to: 0 }], 1000, am4core.ease.circleOut);
        animation.events.on("animationended", function(event){
        animateBullet(event.target.object);
        })
    }

    var colorSet = new am4core.ColorSet();
    imageSeries.data = [
        <?php 
        while ($currentTravel = $travels->fetch()) {
            ?>
            
            {
                "title": "<?php echo json_decode($currentTravel["location"], true)["name"];?>",
                "latitude": <?php echo json_decode($currentTravel["location"], true)["latitude"];?>,
                "longitude": <?php echo json_decode($currentTravel["location"], true)["longitude"];?>,
                "color":colorSet.next(),
                "studentName":  "<?php echo strtoupper($currentTravel["lastname"]).' '.$currentTravel["firstname"]; ?>"
            },
            <?php
    }
    ?>
    ];
    
    });
</script>