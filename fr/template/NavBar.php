<div id="nav-bar">
    <h2 id="t-nb"><?php echo ($isAdmin ? "Admin Dashboard" : "Student DashBoard"); ?></h2>
    <p id="n-nb"><?php echo $userFirstname.' '.strtoupper($userLastname); ?></p>
    <p id="sign-out-button" title="Cliquez-ici pour vous déconnecter" onclick="signOut()"><i class="fas fa-power-off" title="Cliquez-ici pour vous déconnecter"></i></p>
</div>