<?php

require_once('../../vendor/autoload.php');
require_once('../../api/database/User.php');
require_once('../../api/database/Reset.php');
require_once('../../api/database/Travel.php');
require_once('../../api/database/Connection.php');

use Firebase\JWT\JWT;

// JWT secret
$sshValidation = <<<EOD
AAAAB3NzaC1yc2EAAAADAQABAAABgQCvC2I8y99fw+iI5y292Meyagg/JFMm+RK7Z
u3vUKXRVVXMi10KRVITm1XmWvKqJzEDU6419dhoCX+KQ5qg6zqAAB9sppldSYaTOW
86ldPLvIPnej0uNi79dNlRCxxUNAduezfKstQnQx99kV9eR4te7qd7iGj+H3mDGFm
c+VJ0E36Zc4Cs2N8KJUotpb7z3pE28rrOz0nm3zhdo5FKa4Ar+WsdJDxlKAFGH95G
+o8riSAv+a6eTKfI58V2KxQ0iO9thjJ/mKT4Mz08AelyBo/OTMMeI5YLCOR5P6Cct
nrIzCikvA5qTzzQ4VUHNclLK9FWuscfNagmiMAlQggcxxRM8MxYM0ls0Jx4fufFxn
HU5AtZOFiharnNIwO0OefeeDf9nXfGlc8ABt36L3RoNHMqpqW/U9mrrwIxzUYGWL8
6EJQo0dGlZiIRhn1jHUXfcrA53PY237KFOMlmQPI5fSRslKz3z9wX86FpbACKk2Wk
B1FS3du5mPp8WkK8SmCCc70=
EOD;

$sshReset = <<<EOD
AAAAB3NzaC1yc2EAAAADAQABAAABgQDKER9ErCzKKE413khWx4tHAAb0oKo30WChP
mYstj8r33LSt7Q2aBTsM3MkdpVDrMrt9nY3GGsr5lOVxYnTzPrqDeuglX/yVtzVyM
z+95hGIKMefpQEJ0RIsWq1GRE+Kdz1rSoLks7Y68pOXbLGvElPvAGwedjH9V29Zj7
s+MFsSXLZCcgO0X+seZlIIN1LB5A8sOqkssW423BhiTVRSIZiLxxn6DmnwlBvuE4B
uqccxcpeYFC4zrYJnh8VE/+/UVa0wHR9dmatY4XrqUHUPLW/lmPwTRuQGjz1wT1Ay
JPz/hcFjFWLB6tFCwGKXpywtzvigCm+62wqLW0l8+U3YzZBmlfaQYNOJaZnChgymm
KRE7cyWNzkbdQQxLPCsYOmxyaYuGmsri4GODcZ0GQxHhnhiiRnBTF//o8RiMS4kk7
pkGwsSQ2PbnJmIzIe2yQa229btU/3YFTNQulnM1+tsPUCiu/wF42gSFZcMeVMW3HD
tnpNab7z2GPDT0XoySm+/pc=
EOD;

$sshSignIn = <<<EOD
AAAAB3NzaC1yc2EAAAADAQABAAABgQC/OzLG6hDfq4vEvDYzUkKHdCNKQN0nU6NGcm8lxZWbVft4TPnlAaw
AVohQeAURmoicod8z5oHYkeiQDJQYnMcPI/hOllmku20p7ZaEk7fVgXi6b4Xc9/A79kcV7s6umvTgFgU/az
iT0zYS+NGq9KV69kQT/dhHTjnyEb0nIR6gQA/aDd8M3+4bkTiwIgC6Y1bNDESC+ppLZ+QV3gyg4t6FWcsaD
99JYSCPbdEKA11mx2Dnuwa7efWUpq+BZf7BTbYRUxcojF+tFIEEQ7dpMJraHJcmLX0MBZW7HEl8/K3xPpTr
IGDt/W7i0W6PpwUTWUHqCvg/N+KaUi6+5hQBUgIE8k1fpKD1RwGpK0kmVUSGbzz26FZEuPM+wO3rjEb5M0A
54mxvLBn9gs/zEi/Cdblr7WF/rQVkC/LXa8Ej4PBryvnFBsvtjXmf1Mp9IBH84Jm7s7Xxc0E10BezM8TClx
hcXuDTClbVooRNZy/+1+f+1b7HBDRPlcnu+pgBqt2KRH0=
EOD;

define('JWT_SIGN_IN_SECRET' , $sshSignIn);
define('JWT_Validation',$sshValidation);
define('JWT_Reset', $sshReset);

$bufferPage = $realPageToDisplay;
$pageToDisplay = $realPageToDisplay;

if (!isset($pageToDisplay)) {
    $pageToDisplay = "Login.php";
}

$isAuth = false;
$isAdmin = false;
$userFirstname = '';
$userLastname = '';
$userRole = '';
$userRole = '';
$userId = -1;

$scanMessage = "It's not a valid JWT";

if(isset($_COOKIE['AUTH-TSE-SECURED']) && !empty($_COOKIE['AUTH-TSE-SECURED'])) {

    $jwt = $_COOKIE['AUTH-TSE-SECURED'];
    
    $key = JWT_SIGN_IN_SECRET;

    try {
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        // Parse and check if data correspond to those in database
        $decodedArray = get_object_vars($decoded);

        $token = $decodedArray['jti'];      // Unique token
        $expTime = $decodedArray['exp'];    // Expiration time
        $iss = $decodedArray['iss'];        // Server name

        $dataArray = get_object_vars($decodedArray['data']);
        
        $userId = $dataArray['userId'];

        if ($iss === 'TSE-Voyages.fr') {
            $isAuth = true;
            $userFirstname = $dataArray['userFirstname'];
            $userLastname = $dataArray['userLastname'];
            $userRole = $dataArray['userRole'];
            // In future check is user is an admin with query
            $isAdmin = ($userRole === 'Admin' ? true : false);
            $scanMessage = "JWT is authenticated";
        }
    } catch (Firebase\JWT\ExpiredException $e) {
        $scanMessage = "JWT is expired";
    } catch (Firebase\JWT\BeforeValidException $e) {
        $scanMessage = "JWT can only be used between nbf and iat";
    } catch (Firebase\JWT\SignatureInvalidException $e) {
        $scanMessage = "Signature is not correct";
    }
}

if ($pageToDisplay === "Verify.php") {
    if(isset($_GET['JWT_Validation']) && !empty($_GET['JWT_Validation'])) {

        $jwt = $_GET['JWT_Validation'];
        
        $key = JWT_Validation;
    
        try {
            $decoded = JWT::decode($jwt, $key, array('HS256'));
            // Parse and check if data correspond to those in database
            $decodedArray = get_object_vars($decoded);
    
            $token = $decodedArray['jti'];      // Unique token
            $expTime = $decodedArray['exp'];    // Expiration time
            $iss = $decodedArray['iss'];        // Server name
            
            $dataArray = get_object_vars($decodedArray['data']);
            
            $firstname = $dataArray['userFirstname'];
            $lastname = $dataArray['userLastname'];
            $role = $dataArray['userPromotion'];
    
            $user = new User();
    
            $userId = $user->getUserId($dataArray['userEmail']);
            
            // If JWT is valid and timeExpiration is under timeStamp --> Auto log in the user
            if ($iss === 'TSE-Voyages.fr' && $expTime > time() && $user->confirmRegistrationOn($token)) {
                $scanMessage = "Validated account";
            } else if ($iss === 'TSE-Voyages.fr' && $user->confirmRegistrationOn($jwt) && $expTime <= time()) {     // JWT is expired
                $scanMessage = "JWT is expired by test";
            } else {
                $scanMessage = "The JWT is no longer exist";
            }
        } catch (Firebase\JWT\ExpiredException $e) {
            $scanMessage = "JWT is expired";
        } catch (Firebase\JWT\BeforeValidException $e) {
            $scanMessage = "JWT is not correct";
        } catch (Firebase\JWT\SignatureInvalidException $e) {
            $scanMessage = "JWT is not correct";
        }
    }
}

if ($pageToDisplay === "Reset.php") {
    if(isset($_GET['JWT_Reset']) && !empty($_GET['JWT_Reset'])) {

        $jwt = $_GET['JWT_Reset'];
        
        $key = JWT_Reset;
    
        try {
            $decoded = JWT::decode($jwt, $key, array('HS256'));
            // Parse and check if data correspond to those in database
            $decodedArray = get_object_vars($decoded);
    
            $token = $decodedArray['jti'];      // Unique token
            $expTime = $decodedArray['exp'];    // Expiration time
            $iss = $decodedArray['iss'];        // Server name
    
            $dataArray = get_object_vars($decodedArray['data']);
    
            $user = new User();
            $reset = new Reset();
    
            $userId = $user->getUserId($dataArray['userEmail']);

            // If JWT is valid and timeExpiration is under timeStamp --> Auto log in the user
            if ($iss === 'TSE-Voyages.fr' && $expTime > time() && $reset->isValidToken($token, $userId)) {
                $scanMessage = "JWT is authentifcated";
            } else {
                $scanMessage = "The JWT is no longer exist";
            }
        } catch (Firebase\JWT\ExpiredException $e) {
            $scanMessage = "JWT is expired";
        } catch (Firebase\JWT\BeforeValidException $e) {
            $scanMessage = "JWT is not correct";
        } catch (Firebase\JWT\SignatureInvalidException $e) {
            $scanMessage = "JWT is not correct";
        }
    }
}

$adminPages = ['Admin-Dashboard.php'];
$onlinePages = ['Dashboard.php','Admin-Dashboard.php'];

if ($isAuth && !in_array($pageToDisplay, $onlinePages)) {
    if ($isAdmin) {
        $pageToDisplay = 'Admin-Dashboard.php';
    } else {
        $pageToDisplay = 'Dashboard.php';
    }
}

if (!$isAuth && in_array($pageToDisplay, $onlinePages)) {
    $pageToDisplay = "Login.php";
}

$websitePages = [   'Access-Forbidden.php',
                    'Admin-Dashboard.php',
                    'Bad-Request.php',
                    'Dashboard.php',
                    'Login.php',
                    'Method-Not-Allowed.php',
                    'Not-Found.php',
                    'Registration.php',
                    'Reset.php',
                    'Server-Error.php',
                    'Unauthorized.php',
                    'Verify.php'];

$pageTitles = [     'Accès Interdit',
                    'Tableau de bord : Administrateur',
                    'Mauvaise requête',
                    'Tableau de bord',
                    'Connexion',
                    'Méthode non athorisée',
                    'Page introuvable',
                    'Inscription',
                    'Réinitialisation du mot de passe',
                    'Erreur serveur',
                    'Interdiction',
                    'Vérification de compte'];

// Match page and title
$indexPageFound = array_search($pageToDisplay, $websitePages);

// URL Info corresponds to current page
if ($pageToDisplay !== $bufferPage) {
    header('Location:'.$pageToDisplay);
    exit();
}

$pageTitle = 'TSE Voyages : '.$pageTitles[$indexPageFound];

// The head file is included here
require_once('../template/head.html');

?>
<body>
    <script>
        const page = <?php echo json_encode($pageToDisplay); ?>;
        const userId = <?php echo json_encode($userId); ?>;
    </script>
    <?php

    // The right model is included here
    if (file_exists('../model/'.$pageToDisplay)) {
        require_once('../model/'.$pageToDisplay);
    }

    if ($isAuth) {
        require_once('../template/NavBar.php');
    }

    // The right view is included here
    if (file_exists('../view/'.$pageToDisplay)) {
        require_once('../view/'.$pageToDisplay);
    }

    // The script file is included here
    require_once('../template/script.html')

    ?>
</body>