<?php

$travel = new Travel();

$travels = $travel->getAllTravels();

$travelsBis = $travel->getAllTravels();

$travelsThird = $travel->getAllTravels();

$countries = [];
while ($currentTravel = $travelsBis->fetch()) {
    $currentCountry = json_decode($currentTravel['location'],true)['country'];
    if (!in_array($currentCountry, $countries)) {
        array_push($countries, $currentCountry);
    }
}