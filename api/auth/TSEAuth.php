<?php

header("Access-Control-Allow-Origin: http://localhost/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once('../../vendor/autoload.php');

use Firebase\JWT\JWT;

$sshSignIn = <<<EOD
AAAAB3NzaC1yc2EAAAADAQABAAABgQC/OzLG6hDfq4vEvDYzUkKHdCNKQN0nU6NGcm8lxZWbVft4TPnlAaw
AVohQeAURmoicod8z5oHYkeiQDJQYnMcPI/hOllmku20p7ZaEk7fVgXi6b4Xc9/A79kcV7s6umvTgFgU/az
iT0zYS+NGq9KV69kQT/dhHTjnyEb0nIR6gQA/aDd8M3+4bkTiwIgC6Y1bNDESC+ppLZ+QV3gyg4t6FWcsaD
99JYSCPbdEKA11mx2Dnuwa7efWUpq+BZf7BTbYRUxcojF+tFIEEQ7dpMJraHJcmLX0MBZW7HEl8/K3xPpTr
IGDt/W7i0W6PpwUTWUHqCvg/N+KaUi6+5hQBUgIE8k1fpKD1RwGpK0kmVUSGbzz26FZEuPM+wO3rjEb5M0A
54mxvLBn9gs/zEi/Cdblr7WF/rQVkC/LXa8Ej4PBryvnFBsvtjXmf1Mp9IBH84Jm7s7Xxc0E10BezM8TClx
hcXuDTClbVooRNZy/+1+f+1b7HBDRPlcnu+pgBqt2KRH0=
EOD;

define('JWT_SIGN_IN_SECRET' , $sshSignIn);

/**
 * Method to check access rights on roles required
 * 
 * @param Array                     $roleRequired represents role that allows to access to ressource
 *
 * @return Array which contains boolean to indicates access rights and the user id
 */
function controlAccessOnJWTAuth($roleRequired) {
    // Store log message of JWT decode
    $scanMessage = "JWT is missing";

    $userId = 0;
    $accessAuth = false;
    
    if(isset($_COOKIE['AUTH-TSE-SECURED']) && !empty($_COOKIE['AUTH-TSE-SECURED'])) {

        $jwt = $_COOKIE['AUTH-TSE-SECURED'];
        
        $key = JWT_SIGN_IN_SECRET;
    
        try {
            $decoded = JWT::decode($jwt, $key, array('HS256'));
            // Parse and check if data correspond to those in database
            $decodedArray = get_object_vars($decoded);
    
            $token = $decodedArray['jti'];      // Unique token
            $expTime = $decodedArray['exp'];    // Expiration time
            $iss = $decodedArray['iss'];        // Server name
    
            $dataArray = get_object_vars($decodedArray['data']);
            
            $userId = $dataArray['userId'];
    
            if ($iss === 'TSE-Voyages.fr') {
                $scanMessage = "JWT is authenticated";
                $userRole = $dataArray['userRole'];
                if (in_array($userRole,$roleRequired)) {
                    $accessAuth = true;
                }
            }
        } catch (Firebase\JWT\ExpiredException $e) {
            $scanMessage = "JWT is expired";
        } catch (Firebase\JWT\BeforeValidException $e) {
            $scanMessage = "JWT can only be used between nbf and iat";
        } catch (Firebase\JWT\SignatureInvalidException $e) {
            $scanMessage = "Signature is not correct";
        }
    }
    return [$accessAuth,$userId];
}