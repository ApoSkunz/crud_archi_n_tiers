<?php

header("Access-Control-Allow-Origin: http://localhost/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

use Firebase\JWT\JWT;

require_once('../../vendor/autoload.php');
require_once('../database/User.php');
require_once('../database/Connection.php');

//JWT Log secret

$sshSignIn = <<<EOD
AAAAB3NzaC1yc2EAAAADAQABAAABgQC/OzLG6hDfq4vEvDYzUkKHdCNKQN0nU6NGcm8lxZWbVft4TPnlAaw
AVohQeAURmoicod8z5oHYkeiQDJQYnMcPI/hOllmku20p7ZaEk7fVgXi6b4Xc9/A79kcV7s6umvTgFgU/az
iT0zYS+NGq9KV69kQT/dhHTjnyEb0nIR6gQA/aDd8M3+4bkTiwIgC6Y1bNDESC+ppLZ+QV3gyg4t6FWcsaD
99JYSCPbdEKA11mx2Dnuwa7efWUpq+BZf7BTbYRUxcojF+tFIEEQ7dpMJraHJcmLX0MBZW7HEl8/K3xPpTr
IGDt/W7i0W6PpwUTWUHqCvg/N+KaUi6+5hQBUgIE8k1fpKD1RwGpK0kmVUSGbzz26FZEuPM+wO3rjEb5M0A
54mxvLBn9gs/zEi/Cdblr7WF/rQVkC/LXa8Ej4PBryvnFBsvtjXmf1Mp9IBH84Jm7s7Xxc0E10BezM8TClx
hcXuDTClbVooRNZy/+1+f+1b7HBDRPlcnu+pgBqt2KRH0=
EOD;

define('JWT_SIGN_IN_SECRET' , $sshSignIn);

$action = (isset($_POST['action']) && !empty($_POST['action']) ? $_POST['action'] : null);

switch($action) {
    case 'toLogIn':
        toLogIn();
    break;
    case 'signOut':
        signOut();
    break;
    default:
        header('Location:/405.php');
    exit;
}

<<<<<<< HEAD
/**
 * Method to sign in
 *
 * @return JSON for AJAX response to JavaScript
 */
=======
//Function to sign in user
>>>>>>> Documentation JS/PHP + errors messages with ajax for travel
function toLogIn() {

    if (isset($_POST['email']) && !empty($_POST['email']) &&
    isset($_POST['password']) && !empty($_POST['password'])) {

        $secureEmail = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['email']));
        $securePassword = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['password']));

        $user = new User();

        if (!filter_var($secureEmail, FILTER_VALIDATE_EMAIL)) {
            echo json_encode(array(
                'code' => 403,
                'status' => 'error',
                'message' => 'Email is not valid',
            ));
        } elseif (!($user->doesCredentialsExist($secureEmail, $securePassword))) {
            echo json_encode(array(
                'code' => 403,
                'status' => 'error',
                'message' => 'Information are incorrect'
            ));
        } else {
            
            

            $userInfo = $user->getMainUserInfo($secureEmail)->fetch();

            $secureFirstname = $userInfo['firstname'];
            $secureLastname = $userInfo['lastname'];
            $secureRole = $userInfo['role'];
            $secureId = $user->getUserId($secureEmail);

            // Generate our JWT to confirm registration
            $issuedAt = time();
            $tokenId = bin2hex(openssl_random_pseudo_bytes(16));
            $serverName = "TSE-Voyages.fr";
            // None delay
            $notBefore = $issuedAt;
            // JWT valid time before expiration --> 48h
            $expirationTime = $issuedAt + 2*24*60*60;
            
            $payload = [
                'iat'  => $issuedAt,         // Issued at: time when the token was generated
                'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
                'iss'  => $serverName,       // Issuer
                'nbf'  => $notBefore,        // Not before
                'exp'  => $expirationTime,   // Expire
                'data' => [                  // Data related to the signer user
                    'userLastname'  => $secureLastname,     // Lastname
                    'userFirstname' => $secureFirstname,    // Firstname
                    'userRole'      => $secureRole,         // Promotion
                    'userId'        => $secureId
                ]
            ];

            $key = JWT_SIGN_IN_SECRET;
            $alg = 'HS256';
            $secureJWT = JWT::encode($payload, $key);

            $date = new DateTime();
            $date->setTimestamp($expirationTime);
            $expiredDate = $date->format('Y-m-d H:i:s');
            $connections = new Connection();

            if ($connections->storeNewAuthentification($secureId, $tokenId, $expiredDate)) {
                setcookie("AUTH-TSE-SECURED", $secureJWT, $expirationTime, "/", null, false, true);
                echo json_encode(array(
                    'code' => 200,
                    'status' => 'success',
                    'message' => "Credentials are correct",
                    'role' => $secureRole
                ));
            } else {
                echo json_encode(array(
                    'code' => 403,
                    'status' => 'error',
                    'message' => "Server error",
                ));
            }
        }
    } else {
        echo json_encode(array(
            'code' => 403,
            'status' => 'error',
            'message' => 'Data are empty'
        ));
    }
}

/**
 * Method to sign out
 *
 * @return JSON for AJAX response to JavaScript
 */
function signOut() {
    if (isset($_COOKIE['AUTH-TSE-SECURED']) && !empty($_COOKIE['AUTH-TSE-SECURED'])) {
        $jwt = $_COOKIE['AUTH-TSE-SECURED'];
        
        $key = JWT_SIGN_IN_SECRET;
    
        try {
            $decoded = JWT::decode($jwt, $key, array('HS256'));
            // Parse and check if data correspond to those in database
            $decodedArray = get_object_vars($decoded);
            
            $token = $decodedArray['jti'];      // Unique token
            $iss = $decodedArray['iss'];        // Server name
    
            $dataArray = get_object_vars($decodedArray['data']);
            
            $userId = $dataArray['userId'];

            if ($iss === 'TSE-Voyages.fr') {
                unset($_COOKIE["AUTH-TSE-SECURED"]);
                setcookie("AUTH-TSE-SECURED", '', -3600, "/", null, false, true);
                echo json_encode(array(
                    'code' => 200,
                    'status' => 'success',
                    'message' => "Sign out was successful",
                ));
            }
        } catch (Firebase\JWT\ExpiredException $e) {
            $scanMessage = "JWT is expired";
            echo json_encode(array(
                'code' => 400,
                'status' => 'error',
                'message' => $scanMessage,
            ));
        } catch (Firebase\JWT\BeforeValidException $e) {
            $scanMessage = "JWT can only be used between nbf and iat";
            echo json_encode(array(
                'code' => 400,
                'status' => 'error',
                'message' => $scanMessage,
            ));
        } catch (Firebase\JWT\SignatureInvalidException $e) {
            $scanMessage = "Signature is not correct";
            echo json_encode(array(
                'code' => 400,
                'status' => 'error',
                'message' => $scanMessage,
            ));
        }
    } else {
        echo json_encode(array(
            'code' => 400,
            'status' => 'error',
            'message' => "You are not authenticate",
        ));
    }
}