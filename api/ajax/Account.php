<?php

header("Access-Control-Allow-Origin: http://localhost/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use Firebase\JWT\JWT;

require_once('../../vendor/autoload.php');
require_once('../database/User.php');
require_once('../database/Reset.php');

// JWT secret
$sshReset = <<<EOD
AAAAB3NzaC1yc2EAAAADAQABAAABgQDKER9ErCzKKE413khWx4tHAAb0oKo30WChP
mYstj8r33LSt7Q2aBTsM3MkdpVDrMrt9nY3GGsr5lOVxYnTzPrqDeuglX/yVtzVyM
z+95hGIKMefpQEJ0RIsWq1GRE+Kdz1rSoLks7Y68pOXbLGvElPvAGwedjH9V29Zj7
s+MFsSXLZCcgO0X+seZlIIN1LB5A8sOqkssW423BhiTVRSIZiLxxn6DmnwlBvuE4B
uqccxcpeYFC4zrYJnh8VE/+/UVa0wHR9dmatY4XrqUHUPLW/lmPwTRuQGjz1wT1Ay
JPz/hcFjFWLB6tFCwGKXpywtzvigCm+62wqLW0l8+U3YzZBmlfaQYNOJaZnChgymm
KRE7cyWNzkbdQQxLPCsYOmxyaYuGmsri4GODcZ0GQxHhnhiiRnBTF//o8RiMS4kk7
pkGwsSQ2PbnJmIzIe2yQa229btU/3YFTNQulnM1+tsPUCiu/wF42gSFZcMeVMW3HD
tnpNab7z2GPDT0XoySm+/pc=
EOD;

define('JWT_Reset', $sshReset);

$action = (isset($_POST['action']) && !empty($_POST['action']) ? $_POST['action'] : null);

switch($action) {
    case 'sendResetPasswordMail':
        sendResetPasswordMail();
    break;
    case 'resetPassword':
        changePassword();
    break;
    default:
        header('Location:/405.php');
    exit;
}

/**
 * Method to send reset password mail
 * 
 *
 * @return JSON for AJAX response to JavaScript
 */
function sendResetPasswordMail() {
    if (isset($_POST['email']) && !empty($_POST['email'])) {

        $secureEmail = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['email']));

        $user = new User();

        if ($user->getMainUserInfo($secureEmail)->rowCount() > 0) {
            $reset = new Reset();

            $userInfo = $user->getMainUserInfo($secureEmail)->fetch();

            $secureFirstname = $userInfo['firstname'];
            $secureLastname = $userInfo['lastname'];
            $secureRole = $userInfo['role'];
            $secureId = $user->getUserId($secureEmail);

            // Generate our JWT to confirm registration
            $issuedAt = time();
            $tokenId = bin2hex(openssl_random_pseudo_bytes(16));
            $serverName = "TSE-Voyages.fr";
            // None delay
            $notBefore = $issuedAt;
            // JWT valid time before expiration --> 48h
            $expirationTime = $issuedAt + 24*60*60*2;

            $payload = [
                'iat'  => $issuedAt,         // Issued at: time when the token was generated
                'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
                'iss'  => $serverName,       // Issuer
                'nbf'  => $notBefore,        // Not before
                'exp'  => $expirationTime,   // Expire
                'data' => [                  // Data related to the signer user
                    'userLastname'  => $secureLastname,     // Lastname
                    'userFirstname' => $secureFirstname,    // Firstname
                    'userPromotion' => $secureRole,         // Promotion
                    'userEmail'     => $secureEmail         // Email
                ]
            ];

            $key = JWT_Reset;
            $alg = 'HS256';
            $secureJWT = JWT::encode($payload, $key);

            $date = new DateTime();
            $createdDate = $date->format('Y-m-d H:i:s');

            if ($reset->newResetOn($tokenId, $secureId, $createdDate) && sendResetMail($secureFirstname, $secureLastname, $secureEmail, $secureRole, $secureJWT)) {
                echo json_encode(array(
                    'code' => 200,
                    'status' => 'success',
                    'message' => "Mail has been sent",
                ));
            } else {
                echo json_encode(array(
                    'code' => 403,
                    'status' => 'success',
                    'message' => "Mail couldd not be send",
                ));
            }
        } else {
            echo json_encode(array(
                'code' => 403,
                'status' => 'error',
                'message' => 'Mail was not found'
            ));
        }
    } else {
        echo json_encode(array(
            'code' => 403,
            'status' => 'error',
            'message' => 'Data are empty'
        ));
    }
}

/**
 * Method to send mail for reset password
 * 
 * @param Data                              $Data which uses to write email to user with his information
 *
 * @return Boolean which indicates if mail has been sent
 */
function sendResetMail($secureFirstname, $secureLastname, $secureEmail, $secureRole, $secureJWT) {
    // HTML Email Format
    $messageToUserHTMLVersion = '
        <html>
        <head>
        <style>

        p
        {
            margin-top: 1%;
            margin-bottom: 1%;
            color: inherit;
        }

        a
        {
            margin-top: 5%;
            margin-bottom: 5%;
        }

        .email-in-tse-mail a
        {
            color: #242424;
        }
        
        .button-tse-style-1
        {
            display: inline-block;
            width: 50%;
            margin: auto 25%;
            text-align: center;
            background: #242424;
            border-radius: 0.25rem;
            border: 0.125rem solid #f2c56b;
            padding: 0.5rem 1rem;
            color: #fdfdfd;
            cursor: pointer;
            transition: all 1s ease-in-out;
        }

        #button-p a
        {
            color: #fdfdfd;
        }

        #button-p a:hover{
            color: #242424;
        }

        .button-tse-style-1:hover
        {
            color: #242424;
            background: #f2f2f2;
            border-color: #242424;
            transition: all 1s ease-in-out;
        }

        a:link, a:active, a:visited
        {
            color: inherit;
        }

        </style>
        </head>
        <body style="width: 100%; background: #242424;">
            <div style="width:80%; padding: 2.5% 5%; margin: 0% 5%; background: #fffdf7; color: #242424; font-family: Arial, Helvetica, sans-serif; font-size: 1.2em;">
                <p style="margin-top: 2.5%; color: #242424;">Bonjour '.$secureFirstname.' '.$secureLastname.' (promotion '.$secureRole.'),</p>
                <p class="email-in-tse-mail">Vous avez demandé la réinitialisation de votre mot de passe.</p>
                <p>Afin de réinitialiser votre mot de passe merci de cliquer sur le bouton ou sur le lien ci-dessous <strong>valable 48h</strong> et de suivre les étapes:</p>
                <p id="button-p" style="width: 100%;"><a href="http://localhost/fr/controller/Reset.php?JWT_Reset='.$secureJWT.'" title="Cliquez-ici pour valider votre inscription" class="button-tse-style-1">Valider</a></p>
                <a href="http://localhost/fr/controller/Reset.php?JWT_Reset='.$secureJWT.'">http://localhost/fr/controller/Reset.php?JWT_Reset='.$secureJWT.'</a>
                <p style="margin-top: 5%;">Nous espérons que vous apprécierez les nouvelles fonctionnalités accessibles lors de la connexion à votre compte.</p>
                <p>Enfin, nous vous rappelons que nous sécurisons au mieux vos données. Cependant, ne divulguez jamais vos identifiants de connexion à une tierce personne.</p>
                <div style="text-align: center; font-size: 0.9em;">
                    <p>Cordialement,<br/>L\'équipe Audemard-Solane</p>
                    <p class="email-in-tse-mail">Ce mail est généré automatiquement. Veuillez ne pas répondre s\'il vous plaît.</p>
                </div>
            </div>
        </body>';
    
    // Text Email Format
    $messageToUserTextVersion = <<<ENDHEREDOC
Bonjour $secureFirstname $secureLastname (promotion $secureRole),
Merci d'avoir créé un compte sur TSE Voyages.
Afin de valider votre inscription merci de cliquer sur le lien ci-dessous valable 48h :
http://localhost/fr/controller/Reset.php?JWT_Reset=$secureJWT
Nous espérons que vous apprécierez les nouvelles fonctionnalités accessibles lors de la connexion à votre compte.
Enfin, nous vous rappelons que nous sécurisons au mieux vos données. Cependant, ne divulguez jamais vos identifiants de connexion à une tierce personne.
Cordialement, l'équipe Audemard-Solane
Ce mail est généré automatiquement. Veuillez ne pas répondre s'il vous plaît.
ENDHEREDOC;
    
    // Mail subject
    $registerSubject = "Identifiants oubliés";
    
    try {
        // Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);
        
        //Server settings
        //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                        // Enable verbose debug output
        $mail->CharSet = 'UTF-8';                                       // Charset UTF-8
        $mail->Encoding = 'base64';                                     // Encoded on 64bits
        $mail->isSMTP();                                                // Send using SMTP
        $mail->Host       = 'smtp.ionos.fr';                           // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                       // Enable SMTP authentication
        $mail->Username   = 'tse-voyages@crabitanbellevue.fr';           // SMTP username
        $mail->Password   = 'Aa1@jq9iU';                                // SMTP password
        $mail->SMTPSecure = 'ssl';                                      // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 465;                                        // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
        $mail->WordWrap = 50;                                           // Wrapped every 50
        
        //Recipients
        $mail->setFrom('no.reply.tse.voyages@gmail.com', 'TSE Voyages');
        $mail->addAddress($secureEmail, $secureFirstname.' '.$secureLastname);     // Add a recipient
        
        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $registerSubject;
        $mail->Body    = $messageToUserHTMLVersion;
        $mail->AltBody = $messageToUserTextVersion;
        
        //Send
        $mail->send();
        $mail->SmtpClose();
        
        return true;
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        return false;
    }
}

/**
 * Method to change password
 *
 * @return JSON for AJAX response to JavaScript
 */
function changePassword() {
    if (isset($_POST['password']) && !empty($_POST['password']) &&
    isset($_POST['userId']) && !empty($_POST['userId'])) {

        $securePassword = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['password']));
        $secureUserId = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['userId']));

        $secureHashedPassword = password_hash($securePassword, PASSWORD_BCRYPT);

        $user = new User();

        if ($user->changePassword($secureHashedPassword, $secureUserId)) {
            echo json_encode(array(
                'code' => 200,
                'status' => 'success',
                'message' => 'Password has been changed'
            ));
        } else {
            echo json_encode(array(
                'code' => 403,
                'status' => 'error',
                'message' => 'Server error'
            ));
        }
    } else {
        echo json_encode(array(
            'code' => 403,
            'status' => 'error',
            'message' => 'Data are empty'
        ));
    }
}