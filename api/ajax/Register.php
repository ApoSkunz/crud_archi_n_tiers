<?php

header("Access-Control-Allow-Origin: http://localhost/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use Firebase\JWT\JWT;

require_once('../../vendor/autoload.php');
require_once('../database/User.php');

// JWT secret
$sshValidation = <<<EOD
AAAAB3NzaC1yc2EAAAADAQABAAABgQCvC2I8y99fw+iI5y292Meyagg/JFMm+RK7Z
u3vUKXRVVXMi10KRVITm1XmWvKqJzEDU6419dhoCX+KQ5qg6zqAAB9sppldSYaTOW
86ldPLvIPnej0uNi79dNlRCxxUNAduezfKstQnQx99kV9eR4te7qd7iGj+H3mDGFm
c+VJ0E36Zc4Cs2N8KJUotpb7z3pE28rrOz0nm3zhdo5FKa4Ar+WsdJDxlKAFGH95G
+o8riSAv+a6eTKfI58V2KxQ0iO9thjJ/mKT4Mz08AelyBo/OTMMeI5YLCOR5P6Cct
nrIzCikvA5qTzzQ4VUHNclLK9FWuscfNagmiMAlQggcxxRM8MxYM0ls0Jx4fufFxn
HU5AtZOFiharnNIwO0OefeeDf9nXfGlc8ABt36L3RoNHMqpqW/U9mrrwIxzUYGWL8
6EJQo0dGlZiIRhn1jHUXfcrA53PY237KFOMlmQPI5fSRslKz3z9wX86FpbACKk2Wk
B1FS3du5mPp8WkK8SmCCc70=
EOD;

define('JWT_Validation',$sshValidation);

$action = (isset($_POST['action']) && !empty($_POST['action']) ? $_POST['action'] : null);

switch($action) {
    case 'registerNewAccount':
        registerNewAccount();
    break;
    case 'sendNewMail':
        sendNewMail();
    break;
    default:
        header('Location:/405.php');
    exit;
}

/**
 * Method to register new account
 *
 * @return JSON for AJAX response to JavaScript
 */
function registerNewAccount() {
    if (isset($_POST['email']) && !empty($_POST['email']) &&
    isset($_POST['password']) && !empty($_POST['password']) &&
    isset($_POST['passwordConfirmation']) && !empty($_POST['passwordConfirmation']) &&
    isset($_POST['firstname']) && !empty($_POST['firstname']) &&
    isset($_POST['lastname']) && !empty($_POST['lastname']) &&
    isset($_POST['role']) && !empty($_POST['role'])) {

        $secureEmail = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['email']));
        $securePassword = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['password']));
        $securePasswordConfirmation = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['passwordConfirmation']));
        $secureFirstname = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['firstname']));
        $secureLastname = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['lastname']));
        $secureRole = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['role']));

        $user = new User();

        $authorizedRoles = ["CITISE1", "CITISE2", "DCMIN1", "DCMIN2", "DE1", "DE2", "DE3", "DTA", "FISE1", "FISE2", "FISE3", "IPSI1", "IPSI2", "IPSI3", "L3", "SMW"];

        if (!filter_var($secureEmail, FILTER_VALIDATE_EMAIL)) {
            echo json_encode(array(
                'code' => 403,
                'status' => 'error',
                'message' => 'Email is not valid',
            ));
        } elseif ($user->doesEmailExist($secureEmail)) {
            echo json_encode(array(
                'code' => 403,
                'status' => 'error',
                'message' => 'Email already exists'
            ));
        } elseif ($securePassword !== $securePasswordConfirmation) {
            echo json_encode(array(
                'code' => 403,
                'status' => 'error',
                'message' => "Passwords don't match",
            ));
        } elseif (!in_array($secureRole, $authorizedRoles)) {
            echo json_encode(array(
                'code' => 403,
                'status' => 'error',
                'message' => "Role is not authorized",
            ));
        } else {

            $secureHashedPassword = password_hash($securePassword, PASSWORD_BCRYPT);

            // Generate our JWT to confirm registration
            $issuedAt = time();
            $tokenId = bin2hex(openssl_random_pseudo_bytes(16));
            $serverName = "TSE-Voyages.fr";
            // None delay
            $notBefore = $issuedAt;
            // JWT valid time before expiration --> 48h
            $expirationTime = $issuedAt + 24*60*60*2;

            $payload = [
                'iat'  => $issuedAt,         // Issued at: time when the token was generated
                'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
                'iss'  => $serverName,       // Issuer
                'nbf'  => $notBefore,        // Not before
                'exp'  => $expirationTime,   // Expire
                'data' => [                  // Data related to the signer user
                    'userLastname'  => $secureLastname,     // Lastname
                    'userFirstname' => $secureFirstname,    // Firstname
                    'userPromotion' => $secureRole,         // Promotion
                    'userEmail'     => $secureEmail         // Email
                ]
            ];

            $key = JWT_Validation;
            $alg = 'HS256';
            $secureJWT = JWT::encode($payload, $key);

            $date = new DateTime();
            $createdDate = $date->format('Y-m-d H:i:s');

            if ($user->createNewUser($secureLastname, $secureFirstname, $secureEmail, $secureHashedPassword, $secureRole, $tokenId, $createdDate)) {
                if (sendRegistrationMail($secureFirstname, $secureLastname, $secureEmail, $secureRole, $secureJWT)) {
                    echo json_encode(array(
                        'code' => 200,
                        'status' => 'success',
                        'message' => "New user has been registered",
                    ));
                } else {
                    echo json_encode(array(
                        'code' => 200,
                        'status' => 'success',
                        'message' => "The confirmation email was not send",
                    ));
                }
            } else {
                echo json_encode(array(
                    'code' => 403,
                    'status' => 'error',
                    'message' => "An error has occured",
                ));
            }
        }
    } else {
        echo json_encode(array(
            'code' => 403,
            'status' => 'error',
            'message' => 'Data are empty'
        ));
    }
}

/**
 * Method to send new activation link
 *
 * @return JSON for AJAX response to JavaScript
 */
function sendNewMail() {
    if (isset($_POST['email']) && !empty($_POST['email'])) {

        $secureEmail = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['email']));

        $user = new User();

        if ($user->getMainUserInfo($secureEmail)->rowCount() > 0) {
            if ($user->isAlreadyActivated($secureEmail)) {
                echo json_encode(array(
                    'code' => 200,
                    'status' => 'error',
                    'message' => "Account is already activated",
                ));
            } else {
                $userInfo = $user->getMainUserInfo($secureEmail)->fetch();

                $secureFirstname = $userInfo['firstname'];
                $secureLastname = $userInfo['lastname'];
                $secureRole = $userInfo['role'];

                // Generate our JWT to confirm registration
                $issuedAt = time();
                $tokenId = bin2hex(openssl_random_pseudo_bytes(16));
                $serverName = "TSE-Voyages.fr";
                // None delay
                $notBefore = $issuedAt;
                // JWT valid time before expiration --> 48h
                $expirationTime = $issuedAt + 24*60*60*2;

                $payload = [
                    'iat'  => $issuedAt,         // Issued at: time when the token was generated
                    'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
                    'iss'  => $serverName,       // Issuer
                    'nbf'  => $notBefore,        // Not before
                    'exp'  => $expirationTime,   // Expire
                    'data' => [                  // Data related to the signer user
                        'userLastname'  => $secureLastname,     // Lastname
                        'userFirstname' => $secureFirstname,    // Firstname
                        'userPromotion' => $secureRole,         // Promotion
                        'userEmail'     => $secureEmail         // Email
                    ]
                ];

                $key = JWT_SECRET;
                $alg = 'HS256';
                $secureJWT = JWT::encode($payload, $key);

                if ($user->updateAuthorizationToken($tokenId, $secureEmail) && sendRegistrationMail($secureFirstname, $secureLastname, $secureEmail, $secureRole, $secureJWT)) {
                    echo json_encode(array(
                        'code' => 200,
                        'status' => 'success',
                        'message' => "Mail has been sent",
                    ));
                } else {
                    echo json_encode(array(
                        'code' => 403,
                        'status' => 'success',
                        'message' => "Mail couldd not be send",
                    ));
                }
            }
        } else {
            echo json_encode(array(
                'code' => 403,
                'status' => 'error',
                'message' => 'Mail was not found'
            ));
        }
    } else {
        echo json_encode(array(
            'code' => 403,
            'status' => 'error',
            'message' => 'Data are empty'
        ));
    }
}

/**
 * Method to send mail for activation link to user
 * 
 * @param Data                              $Data which uses to write email to user with his information
 *
 * @return Boolean which indicates if mail has been sent
 */
function sendRegistrationMail($secureFirstname, $secureLastname, $secureEmail, $secureRole, $secureJWT) {
    
    // HTML Email Format
    $messageToUserHTMLVersion = '
        <html>
        <head>
        <style>

        p
        {
            margin-top: 1%;
            margin-bottom: 1%;
            color: inherit;
        }

        a
        {
            margin-top: 5%;
            margin-bottom: 5%;
        }

        .email-in-tse-mail a
        {
            color: #242424;
        }
        
        .button-tse-style-1
        {
            display: inline-block;
            width: 50%;
            margin: auto 25%;
            text-align: center;
            background: #242424;
            border-radius: 0.25rem;
            border: 0.125rem solid #f2c56b;
            padding: 0.5rem 1rem;
            color: #fdfdfd;
            cursor: pointer;
            transition: all 1s ease-in-out;
        }

        #button-p a
        {
            color: #fdfdfd;
        }

        #button-p a:hover{
            color: #242424;
        }

        .button-tse-style-1:hover
        {
            color: #242424;
            background: #f2f2f2;
            border-color: #242424;
            transition: all 1s ease-in-out;
        }

        a:link, a:active, a:visited
        {
            color: inherit;
        }

        </style>
        </head>
        <body style="width: 100%; background: #242424;">
            <div style="width:80%; padding: 2.5% 5%; margin: 0% 5%; background: #fffdf7; color: #242424; font-family: Arial, Helvetica, sans-serif; font-size: 1.2em;">
                <p style="margin-top: 2.5%; color: #242424;">Bonjour '.$secureFirstname.' '.$secureLastname.' (promotion '.$secureRole.'),</p>
                <p class="email-in-tse-mail">Merci d\'avoir créé un compte sur TSE Voyages.</p>
                <p>Afin de valider votre inscription merci de cliquer sur le bouton ou sur le lien ci-dessous <strong>valable 48h</strong> :</p>
                <p id="button-p" style="width: 100%;"><a href="http://localhost/fr/controller/Verify.php?JWT_Validation='.$secureJWT.'" title="Cliquez-ici pour valider votre inscription" class="button-tse-style-1">Valider</a></p>
                <a href="http://localhost/fr/controller/Verify.php?JWT_Validation='.$secureJWT.'">http://localhost/fr/controller/Verify.php?JWT_Validation='.$secureJWT.'</a>
                <p>Enfin, nous vous rappelons que nous sécurisons au mieux vos données. Cependant, ne divulguez jamais vos identifiants de connexion à une tierce personne.</p>
                <div style="text-align: center; font-size: 0.9em;">
                    <p>Cordialement,<br/>L\'équipe Audemard-Solane</p>
                    <p class="email-in-tse-mail">Ce mail est généré automatiquement. Veuillez ne pas répondre s\'il vous plaît.</p>
                </div>
            </div>
        </body>';
    
    // Text Email Format
    $messageToUserTextVersion = <<<ENDHEREDOC
Bonjour $secureFirstname $secureLastname (promotion $secureRole),
Merci d'avoir créé un compte sur TSE Voyages.
Afin de valider votre inscription merci de cliquer sur le bouton ou sur le lien ci-dessous valable 48h :
http://localhost/fr/controller/Verify.php?JWT_Validation=$secureJWT
Enfin, nous vous rappelons que nous sécurisons au mieux vos données. Cependant, ne divulguez jamais vos identifiants de connexion à une tierce personne.
Cordialement, l'équipe Audemard-Solane
Ce mail est généré automatiquement. Veuillez ne pas répondre s'il vous plaît.
ENDHEREDOC;
    
    // Mail subject
    $registerSubject = "Validation d'inscription";
    
    try {
        // Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);
        
        //Server settings
        //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                        // Enable verbose debug output
        $mail->CharSet = 'UTF-8';                                       // Charset UTF-8
        $mail->Encoding = 'base64';                                     // Encoded on 64bits
        $mail->isSMTP();                                                // Send using SMTP
        $mail->Host       = 'smtp.ionos.fr';                           // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                       // Enable SMTP authentication
        $mail->Username   = 'tse-voyages@crabitanbellevue.fr';           // SMTP username
        $mail->Password   = 'Aa1@jq9iU';                                // SMTP password
        $mail->SMTPSecure = 'ssl';                                      // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 465;                                        // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
        $mail->WordWrap = 50;                                           // Wrapped every 50
        
        //Recipients
        $mail->setFrom('no.reply.tse.voyages@gmail.com', 'TSE Voyages');
        $mail->addAddress($secureEmail, $secureFirstname.' '.$secureLastname);     // Add a recipient
        
        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $registerSubject;
        $mail->Body    = $messageToUserHTMLVersion;
        $mail->AltBody = $messageToUserTextVersion;
        
        //Send
        $mail->send();
        $mail->SmtpClose();
        
        return true;
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        return false;
    }
}