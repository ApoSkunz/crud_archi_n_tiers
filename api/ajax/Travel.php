<?php

header("Access-Control-Allow-Origin: http://localhost/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use Firebase\JWT\JWT;

require_once('../../vendor/autoload.php');
require_once('../database/Travel.php');
require_once('../auth/TSEAuth.php');

// JWT secret
$sshValidation = <<<EOD
AAAAB3NzaC1yc2EAAAADAQABAAABgQCvC2I8y99fw+iI5y292Meyagg/JFMm+RK7Z
u3vUKXRVVXMi10KRVITm1XmWvKqJzEDU6419dhoCX+KQ5qg6zqAAB9sppldSYaTOW
86ldPLvIPnej0uNi79dNlRCxxUNAduezfKstQnQx99kV9eR4te7qd7iGj+H3mDGFm
c+VJ0E36Zc4Cs2N8KJUotpb7z3pE28rrOz0nm3zhdo5FKa4Ar+WsdJDxlKAFGH95G
+o8riSAv+a6eTKfI58V2KxQ0iO9thjJ/mKT4Mz08AelyBo/OTMMeI5YLCOR5P6Cct
nrIzCikvA5qTzzQ4VUHNclLK9FWuscfNagmiMAlQggcxxRM8MxYM0ls0Jx4fufFxn
HU5AtZOFiharnNIwO0OefeeDf9nXfGlc8ABt36L3RoNHMqpqW/U9mrrwIxzUYGWL8
6EJQo0dGlZiIRhn1jHUXfcrA53PY237KFOMlmQPI5fSRslKz3z9wX86FpbACKk2Wk
B1FS3du5mPp8WkK8SmCCc70=
EOD;

define('JWT_Validation',$sshValidation);

$action = (isset($_POST['action']) && !empty($_POST['action']) ? $_POST['action'] : null);

$controlAccessResponses = controlAccessOnJWTAuth(['Admin']);
$accessAuth = $controlAccessResponses[0];

if (in_array($action, ["searchStudents","updateTravelStatus"]) && !$accessAuth) {
    $action = 'unauthorized';
} elseif (in_array($action, ["addTravel", "deleteTravel"])) {
    $controlAccessResponses = controlAccessOnJWTAuth(["CITISE1", "CITISE2", "DCMIN1", "DCMIN2", "DE1", "DE2", "DE3", "DTA", "FISE1", "FISE2", "FISE3", "IPSI1", "IPSI2", "IPSI3", "L3", "SMW"]);
    $accessAuth = $controlAccessResponses[0];

    if (!$accessAuth) {
        $action = 'unauthorized';
    }
}

switch($action) {
    case 'addTravel':
        addTravel();
    break;
    case 'deleteTravel':
        deleteTravel();
    break;
    case 'searchStudents':
        searchStudents();
    break;
    case 'updateTravelStatus':
        updateTravelStatus();
    break;
    case 'unauthorized':
        header('Location:/401.php');
    exit();
    default:
        header('Location:/405.php');
    exit;
}

//Function to add a travel
function addTravel(){

    if (isset($_POST['city']) && !empty($_POST['city']) &&
    isset($_POST['date_begin']) && !empty($_POST['date_begin']) &&
    isset($_POST['date_end']) && !empty($_POST['date_end']) &&
    isset($_POST['userId']) && !empty($_POST['userId'])) {

        $secureCity = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['city']));
        $secureDateBegin = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['date_begin']));
        $secureDateEnd = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['date_end']));
        $secureUserId = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['userId']));
        
        $apiURL = "http://api.positionstack.com/v1/forward?access_key=2ae22f5f53d00d068f4f88ede8c4ec06&query=".$secureCity;

        $response = file_get_contents($apiURL);
        $locationInJSON = json_decode($response, true)['data'];

        if ($locationInJSON == []) {
            echo json_encode(array(
                'code' => 403,
                'status' => 'error',
                'message' => 'Location is empty'
            ));
        }
        else {
            if($secureDateBegin > $secureDateEnd){
                echo json_encode(array(
                    'code' => 403,
                    'status' => 'error',
                    'message' => 'DateBegin cannot be > than DateEnd'
                ));
            } else {
                $travel = new Travel();
                $status = "En attente";

                if ($travel->createNewTravel($secureDateBegin, $secureDateEnd, json_encode($locationInJSON[0]), $status, $secureUserId)) {
                    echo json_encode(array(
                        'code' => 200,
                        'status' => 'success',
                        'message' => "New travel has been registered"
                    ));
                } else {
                    echo json_encode(array(
                        'code' => 403,
                        'status' => 'error',
                        'message' => "Server error",
                    ));
                }
            }
        }
    } else {
        echo json_encode(array(
            'code' => 403,
            'status' => 'error',
            'message' => 'Data are empty'
        ));
    }
}

//Function to delete a travel
function deleteTravel(){

    if (isset($_POST['travelId']) && !empty($_POST['travelId']) &&
    isset($_POST['userId']) && !empty($_POST['userId'])){

        $secureTravelId = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['travelId']));
        $secureUserId = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['userId']));

        $travel = new Travel();

        if(!($travel->verifyTravelIdAndUserId($secureTravelId, $secureUserId))){
            echo json_encode(array(
                'code' => 403,
                'status' => 'error',
                'message' => 'No travels found with this id'
            ));
        }
        else{

            if($travel->deleteTravelById($secureTravelId)){
                echo json_encode(array(
                    'code' => 200,
                    'status' => 'success',
                    'message' => "Travel has been deleted"
                ));
            }
            else{
                echo json_encode(array(
                    'code' => 403,
                    'status' => 'error',
                    'message' => "Server error",
                ));
            }
        }
    }
    else{
        echo json_encode(array(
            'code' => 403,
            'status' => 'error',
            'message' => 'Data are empty'
        ));
    }
}

//Function to search students
function searchStudents() {
    if ((isset($_POST['student']) && !empty($_POST['student'])) ||
    (isset($_POST['country']) && !empty($_POST['country'])) ||
    (isset($_POST['promotion']) && !empty($_POST['promotion'])) ||
    (isset($_POST['end_date']) && !empty($_POST['end_date'])) ||
    (isset($_POST['begin_date']) && !empty($_POST['begin_date']))) {
        
        $studentName = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['student']));
        $country = (!empty($_POST['country']) ? '%'.str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['country'])).'%' : '');
        $promotion = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['promotion']));
        $end_date = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['end_date']));
        $begin_date = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['begin_date']));

        $travel = new Travel();

        if ($travel->specificSearch($studentName, $country, $promotion, $end_date, $begin_date)) {
            $travels = $travel->specificSearch($studentName, $country, $promotion, $end_date, $begin_date);
            $travelsBis = [];
            while ($currentTravel = $travels->fetch()) {
                $buffer = [];
                array_push($buffer, $currentTravel["lastname"]);
                array_push($buffer, $currentTravel['role']);
                array_push($buffer, json_decode($currentTravel["location"],true)["name"]);
                array_push($buffer, $currentTravel["date_begin"]);
                array_push($buffer, $currentTravel["date_end"]);
                array_push($buffer, $currentTravel["status"]);
                array_push($buffer, json_decode($currentTravel["location"],true)["latitude"]);
                array_push($buffer, json_decode($currentTravel["location"],true)["longitude"]);
                array_push($buffer, strtoupper($currentTravel["lastname"]).' '.$currentTravel['firstname']);

                array_push($travelsBis, $buffer);
            }
            echo json_encode(array(
                'code' => 200,
                'status' => 'success',
                'message' => "Results have been found",
                'data' => $travelsBis
            ));
        } else {
            echo json_encode(array(
                'code' => 403,
                'status' => 'error',
                'message' => "Server error",
            ));
        }
    } else {
        echo json_encode(array(
        'code' => 403,
        'status' => 'error',
        'message' => 'Data are empty'
        ));
    }
}

//Function to update travel's status
function updateTravelStatus() {
    if (isset($_POST['travelId']) && !empty($_POST['travelId'])) {
        $travelId = str_replace(array("\n","\r",PHP_EOL),'',htmlspecialchars($_POST['travelId']));

        $travel = new Travel();

        if ($travel->updateStatus($travelId)) {
            echo json_encode(array(
                'code' => 200,
                'status' => 'success',
                'message' => "Travel have been validated",
                'travelId' => $travelId
            ));
        } else {
            echo json_encode(array(
                'code' => 403,
                'status' => 'error',
                'message' => "Server error",
            ));
        }
    } else {
        echo json_encode(array(
        'code' => 403,
        'status' => 'error',
        'message' => 'Data are empty'
        ));
    }
}