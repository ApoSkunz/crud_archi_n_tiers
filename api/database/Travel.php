<?php

require_once('DatabaseManager.php');

/**
 * Travel implementation based on model defined in TD
 *
 * PHP version 7.4
 *
 * @author   Audemard Lucass <>
 * @author   Solane Alexandre <alexandresolane.web@gmail.com>
 */
class Travel extends DatabaseManager{
 
    /**
     * Object connection to right
     * database
     */
    private $database;

    /**
     * Default table name to manage
     * travels in our database
     */
    private $table_name = "travels";

    /**
     * Build connection to right
     * database and store it in $database
     * private attribute
     */
    public function __construct(){
        $db = new DatabaseManager();
        $this->database = $db->getConnection();
    }
 
    /**
     * Create a new travel in table travels in database on 4 fields
     *
     * @param Date                  $date_begin     The date of beginning of the travel
     * @param Date                  $date_end       The date of ending of the travel
     * @param String                $location       The location of the travel in json with API
     * @param String                $status         The status of the travel
     * @param Integer               $userId         The user id of the travel
     *
     * @return boolean to trust if the travel has been created or not
     */
    public function createNewTravel($date_begin, $date_end, $location, $status, $userId) {
        $statement = $this->database->prepare("INSERT INTO $this->table_name(user_id, date_begin, date_end, location, status) VALUES(:user_id, :date_begin, :date_end, :location, :status)");
        $statement->bindParam(':user_id', $userId);
        $statement->bindParam(':date_begin', $date_begin);
        $statement->bindParam(':date_end', $date_end);
        $statement->bindParam(':location', $location);
        $statement->bindParam(':status', $status);
        
        if($statement->execute()){
            return true;
        }
        return false;
    }

    /**
     * Get travels for on user on it id
     *
     * @param string                $userId         The user id of the travel
     *
     * @return Array which contains user travels
     */
    public function getTravelsOnUserId($userId) {
        $statement = $this->database->prepare("SELECT * FROM $this->table_name WHERE user_id = :user_id");
        $statement->bindParam(':user_id', $userId);
        $statement->execute();

        return $statement;
    }

    /**
     * Delete one travel on it id
     *
     * @param string                $travelId        The travel id
     *
     * @return boolean to indicates if query has been executed
     */
    public function deleteTravelById($travelId){
        $statement = $this->database->prepare("DELETE FROM $this->table_name WHERE id = :travel_id");
        $statement->bindParam(':travel_id', $travelId);

        if($statement->execute()){
            return true;
        }
        return false;

    }

    /**
     * Method to verify if the travel id is associated with the user id
     * 
     * @param String                    $travelId         The travel id
     * @param String                    $userId           The user id
     * 
     * @return boolean to trust if the travel and the user are associated
     */
    public function verifyTravelIdAndUserId($travelId, $userId){
        $statement = $this->database->prepare("SELECT * FROM $this->table_name WHERE id = :travel_id AND user_id = :user_id");
        $statement->bindParam(':travel_id', $travelId);
        $statement->bindParam(':user_id', $userId);
        $statement->execute();

        $count = $statement->rowCount();

        if($count > 0){
            return true;
        }
        return false;
    }
    
    /**
     * Get all travels
     *
     * @return Array which contains travels
     */
    public function getAllTravels() {
        $statement = $this->database->prepare("SELECT $this->table_name.*, lastname, firstname, role FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id");
        $statement->execute();

        return $statement;
    }

    /**
     * Search specific mobilities on multi criterions
     *
     * @param String                $studentName    The student lastname
     * @param String                $country        The country name
     * @param String                $promotion      The promotion
     * @param Date                  $endDate        The end date
     * @param Date                  $beginDate      The begin date
     *
     * @return Array which contains travels
     */
    public function specificSearch($studentName, $country, $promotion, $endDate, $beginDate) {
        if (!empty($studentName)) {
            if (!empty($country)) {
                if (!empty($promotion)) {
                    if (!empty($beginDate)) {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname AND location LIKE :country AND role = :promotion AND DATEDIFF(date_begin > :beginDate AND date_end < :endDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                            $statement->bindParam(':country', $country);
                            $statement->bindParam(':promotion', $promotion);
                            $statement->bindParam(':endDate', $endDate);
                            $statement->bindParam(':beginDate', $beginDate);
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname AND location LIKE :country AND role = :promotion AND date_begin > :beginDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                            $statement->bindParam(':country', $country);
                            $statement->bindParam(':promotion', $promotion);
                            $statement->bindParam(':beginDate', $beginDate);
                        }
                    } else {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname AND location LIKE :country AND role = :promotion AND date_end < :endDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                            $statement->bindParam(':country', $country);
                            $statement->bindParam(':promotion', $promotion);
                            $statement->bindParam(':endDate', $endDate);;
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname AND location LIKE :country AND role = :promotion";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                            $statement->bindParam(':country', $country);
                            $statement->bindParam(':promotion', $promotion);
                        }
                    }
                } else {
                    if (!empty($beginDate)) {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname AND location LIKE :country AND date_begin > :beginDate AND date_end < :endDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                            $statement->bindParam(':country', $country);
                            $statement->bindParam(':endDate', $endDate);
                            $statement->bindParam(':beginDate', $beginDate);
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname AND location LIKE :country AND date_begin > :beginDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                            $statement->bindParam(':country', $country);
                            $statement->bindParam(':beginDate', $beginDate);
                        }
                    } else {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname AND location LIKE :country AND date_end < :endDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                            $statement->bindParam(':country', $country);
                            $statement->bindParam(':endDate', $endDate);;
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname AND location LIKE :country";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                            $statement->bindParam(':country', $country);
                        }
                    }
                }
            } else {
                if (!empty($promotion)) {
                    if (!empty($beginDate)) {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname AND role = :promotion AND date_begin > :beginDate AND date_end < :endDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                            $statement->bindParam(':promotion', $promotion);
                            $statement->bindParam(':endDate', $endDate);
                            $statement->bindParam(':beginDate', $beginDate);
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname AND role = :promotion AND date_begin > :beginDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                            $statement->bindParam(':promotion', $promotion);
                            $statement->bindParam(':beginDate', $beginDate);
                        }
                    } else {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname AND role = :promotion AND date_end < :endDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                            $statement->bindParam(':promotion', $promotion);
                            $statement->bindParam(':endDate', $endDate);;
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname AND role = :promotion";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                            $statement->bindParam(':promotion', $promotion);
                        }
                    }
                } else {
                    if (!empty($beginDate)) {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname AND date_begin > :beginDate AND date_end < :endDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                            $statement->bindParam(':endDate', $endDate);
                            $statement->bindParam(':beginDate', $beginDate);
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname AND date_begin > :beginDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                            $statement->bindParam(':beginDate', $beginDate);
                        }
                    } else {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname AND date_end < :endDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                            $statement->bindParam(':endDate', $endDate);;
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE lastname = :lastname";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':lastname', $studentName);
                        }
                    }
                }
            }
        } else {
            if (!empty($country)) {
                if (!empty($promotion)) {
                    if (!empty($beginDate)) {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE location LIKE :country AND role = :promotion AND date_begin > :beginDate AND date_end < :endDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':country', $country);
                            $statement->bindParam(':promotion', $promotion);
                            $statement->bindParam(':endDate', $endDate);
                            $statement->bindParam(':beginDate', $beginDate);
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE location LIKE :country AND role = :promotion AND date_begin > :beginDate";
                            $statement = $this->database->prepare($query);

                            $statement->bindParam(':country', $country);
                            $statement->bindParam(':promotion', $promotion);
                            $statement->bindParam(':beginDate', $beginDate);
                        }
                    } else {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE location LIKE :country AND role = :promotion AND date_end < :endDate";
                            $statement = $this->database->prepare($query);

                            $statement->bindParam(':country', $country);
                            $statement->bindParam(':promotion', $promotion);
                            $statement->bindParam(':endDate', $endDate);;
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE location LIKE :country AND role = :promotion";
                            $statement = $this->database->prepare($query);

                            $statement->bindParam(':country', $country);
                            $statement->bindParam(':promotion', $promotion);
                        }
                    }
                } else {
                    if (!empty($beginDate)) {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE location LIKE :country AND date_begin > :beginDate AND date_end < :endDate";
                            $statement = $this->database->prepare($query);

                            $statement->bindParam(':country', $country);
                            $statement->bindParam(':endDate', $endDate);
                            $statement->bindParam(':beginDate', $beginDate);
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE location LIKE :country AND date_begin > :beginDate";
                            $statement = $this->database->prepare($query);

                            $statement->bindParam(':country', $country);
                            $statement->bindParam(':beginDate', $beginDate);
                        }
                    } else {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE location LIKE :country AND date_end < :endDate";
                            $statement = $this->database->prepare($query);

                            $statement->bindParam(':country', $country);
                            $statement->bindParam(':endDate', $endDate);;
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE location LIKE :country";
                            $statement = $this->database->prepare($query);

                            $statement->bindParam(':country', $country);
                        }
                    }
                }
            } else {
                if (!empty($promotion)) {
                    if (!empty($beginDate)) {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE role = :promotion AND date_begin > :beginDate AND date_end < :endDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':promotion', $promotion);
                            $statement->bindParam(':endDate', $endDate);
                            $statement->bindParam(':beginDate', $beginDate);
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE role = :promotion AND date_begin > :beginDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':promotion', $promotion);
                            $statement->bindParam(':beginDate', $beginDate);
                        }
                    } else {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE role = :promotion AND date_end < :endDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':promotion', $promotion);
                            $statement->bindParam(':endDate', $endDate);;
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE role = :promotion";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':promotion', $promotion);
                        }
                    }
                } else {
                    if (!empty($beginDate)) {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE date_begin > :beginDate AND date_end < :endDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':endDate', $endDate);
                            $statement->bindParam(':beginDate', $beginDate);
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE date_begin > :beginDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':beginDate', $beginDate);
                        }
                    } else {
                        if (!empty($endDate)) {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE date_end < :endDate";
                            $statement = $this->database->prepare($query);
                            $statement->bindParam(':endDate', $endDate);;
                        } else {
                            $query = "SELECT accounts.*, $this->table_name.* FROM $this->table_name JOIN accounts ON accounts.id = $this->table_name.user_id WHERE 0";
                            $statement = $this->database->prepare($query);

                        }
                    }
                }
            }
        }

        try {
            $statement->execute();
        } catch (Exception $e) {
            echo $e;
        }
        if ($statement->execute()) {
            return $statement;
        }
        return false;
    }

    /**
     * Update status of travel to validate it
     *
     * @param string                $travelId        The travel id
     *
     * @return boolean to indicates if query has been executed
     */
    public function updateStatus($travelId) {
        $statement = $this->database->prepare("UPDATE $this->table_name SET status = 'Validé' WHERE id = :travelId");
        $statement->bindParam('travelId', $travelId);

        if ($statement->execute()) {
            return true;
        }
        return false;
    }
}