<?php

require_once('DatabaseManager.php');

/**
 * User implementation based on model defined in TD
 *
 * PHP version 7.4
 *
 * @author   Audemard Lucass <>
 * @author   Solane Alexandre <alexandresolane.web@gmail.com>
 */
class User extends DatabaseManager{
 
    /**
     * Object connection to right
     * database
     */
    private $database;

    /**
     * Default table name to manage
     * user in our database
     */
    private $table_name = "accounts";

    /**
     * Build connection to right
     * database and store it in $database
     * private attribute
     */
    public function __construct(){
        $db = new DatabaseManager();
        $this->database = $db->getConnection();
    }
 
    /**
     * Create a new user in table accounts in database on 5 fields
     *
     * @param String                    $lastname       The lastname of the user
     * @param String                    $firstname      The firstname of the user
     * @param String                    $email          The email of the user
     * @param String                    $password       The hashed password of the user
     * @param String                    $role           The role of the user
     * @param String                    $authorization  A JWT to validate account by clicking
     *                                                  on a link in the confirmation email
     *
     * @return Boolean to trust if the user has been created or not
     */
    public function createNewUser($lastname, $firstname, $email, $password, $role, $authorization, $createdDate) {
        $statement = $this->database->prepare("INSERT INTO $this->table_name(firstname, lastname, email, password, role , authorization, created_date) VALUES(:firstname, :lastname, :email, :password, :role, :authorization, :created_date)");
        $statement->bindParam(':firstname', $firstname);
        $statement->bindParam(':lastname', $lastname);
        $statement->bindParam(':email', $email);
        $statement->bindParam(':password', $password);
        $statement->bindParam(':role', $role);
        $statement->bindParam(':authorization', $authorization);
        $statement->bindParam(':created_date', $createdDate);

        if($statement->execute()) {
            return true;
        }
        return false;
    }

    /**
     * Method to check credentials matching
     * 
     * @param String                    $email          The email of the user
     * @param String                    $password       The hashed password of the user
     *
     * @return Boolean to indicate if credentials match
     */
    public function doesCredentialsExist($email, $password) {
        $statement = $this->database->prepare("SELECT password FROM $this->table_name WHERE email = :email AND authorization = 'Activated'");
        $statement->bindParam(':email', $email);
        $statement->execute();

        $count = $statement->rowCount();

        if ($count > 0 && password_verify($password, $statement->fetch()["password"])) {
            return true;
        }

        return false;
    }

    /**
     * Method to check if an email exists or not
     * 
     * @param String                    $email          The email to check
     *
     * @return Boolean to indicate if email exists or not
     */
    public function doesEmailExist($email) {
        $statement = $this->database->prepare("SELECT email FROM $this->table_name WHERE email = :email");
        $statement->bindParam(':email', $email);
        $statement->execute();

        $count = $statement->rowCount();

        if ($count === 0) {
            return false;
        }
        return true;
    }

    /**
     * Method to confirm registration or not on token authorization
     * 
     * @param String                    $token          The token stored in DB and in JWT
     *
     * @return Boolean to indicate if registration has been validated or not
     */
    public function confirmRegistrationOn($token) {
        $statement = $this->database->prepare("UPDATE $this->table_name SET authorization = 'Activated' WHERE authorization = :token OR authorization = 'Activated'");
        $statement->bindParam(':token', $token);

        if ($statement->execute();) {
            return true;
        }
        return false;
    }

    /**
     * Method to get user id on it email
     * 
     * @param String                    $email          The email of the user
     *
     * @return Integer of the user
     */
    public function getUserId($email) {
        $statement = $this->database->prepare("SELECT id FROM $this->table_name WHERE email = :user_email");
        $statement->bindParam(':user_email', $email);
        $statement->execute();

        return $statement->fetch()['id'];
    }

    /**
     * Method to get user main info on it email
     * 
     * @param String                    $email          The email of the user
     *
     * @return Array which contains user info
     */
    public function getMainUserInfo($email) {
        $statement = $this->database->prepare("SELECT firstname, lastname, role FROM $this->table_name WHERE email = :email");
        $statement->bindParam(':email', $email);
        $statement->execute();

        return $statement;
    }

    /**
     * Method to update token of and account
     * 
     * @param String                    $token          The new token
     * @param String                    $email          The email of the user
     *
     * @return Boolean to indicate if query has been executed or not
     */
    public function updateAuthorizationToken($token, $email) {
        $statement = $this->database->prepare("UPDATE $this->table_name SET authorization = :token WHERE email = :email");
        $statement->bindParam(':token', $token);
        $statement->bindParam(':email', $email);
        
        if ($statement->execute()) {
            return true;
        }
        return false;
    }

    /**
     * Method to check if an account is already activated or not
     * 
     * @param String                    $email          The email of the user
     *
     * @return Boolean to indicate if the account is already activated or not
     */
    public function isAlreadyActivated($email) {
        $statement = $this->database->prepare("SELECT authorization FROM $this->table_name WHERE email = :email");
        $statement->bindParam(':email', $email);
        $statement->execute();

        $authorizationValue = $statement->fetch()['authorization'];
        if ($authorizationValue === 'Activated') {
            return true;
        }
        return false;
    }

    /**
     * Method to change password on user id
     * 
     * @param String                    $password       The new hashed password of the user
     * @param String                    $userId         The id of the user
     *
     * @return Boolean to indicate if the query has been executed or not
     */
    public function changePassword($password, $userId) {
        $statement = $this->database->prepare("UPDATE $this->table_name SET password = :password WHERE id = :userId");
        $statement->bindParam(':password', $password);
        $statement->bindParam(':userId', $userId);

        if ($statement->execute()) {
            return true;
        }
        return false;
    }

}