<?php

require_once('DatabaseManager.php');

/**
 * Reset implementation based on model defined in TD
 *
 * PHP version 7.4
 *
 * @author   Audemard Lucass <>
 * @author   Solane Alexandre <alexandresolane.web@gmail.com>
 */
class Reset extends DatabaseManager{
 
    /**
     * Object connection to right
     * database
     */
    private $database;

    /**
     * Default table name to manage
     * reset in our database
     */
    private $table_name = "reset";

    /**
     * Build connection to right
     * database and store it in $database
     * private attribute
     */
    public function __construct(){
        $db = new DatabaseManager();
        $this->database = $db->getConnection();
    }

    /**
     * Create a new reset in table reset in database on 3 fields
     *
     * @param String                    $token          The token of JWT reset
     * @param String                    $userId         The id of the user
     * @param DateTime                  $createdDate    The requested date of reset
     *
     * @return Boolean to indicate if query has been executed
     */
    public function newResetOn($token, $userId, $createdDate) {
        $statement = $this->database->prepare("INSERT INTO $this->table_name(user_id, token, requested_on) VALUES(:user_id, :token, :requested_on)");
        $statement->bindParam(':user_id', $userId);
        $statement->bindParam(':token', $token);
        $statement->bindParam(':requested_on', $createdDate);
        
        if ($statement->execute()) {
            return true;
        }
        return false;
    }

    /**
     * Check the validity of the token
     *
     * @param String                    $token          The token of JWT reset
     * @param String                    $userId         The id of the user
     *
     * @return Boolean to indicate if token for this user has been found
     */
    public function isValidToken($token, $userId) {
        $statement = $this->database->prepare("SELECT * from reset WHERE token = :token AND user_id = :userId");
        $statement->bindParam(':token', $token);
        $statement->bindParam(':userId', $userId);
        $statement->execute();
        $isTokenValid = $statement->fetch();

        if (!empty($isTokenValid)) {
            return true;
        }
        return false;
    }
}