<?php

/**
 * DatabaseManager implementation to connect to our DB
 *
 * PHP version 7.4
 *
 * @author   Audemard Lucass <>
 * @author   Solane Alexandre <alexandresolane.web@gmail.com>
 */
class DatabaseManager {
 
    // get the database connection
    protected function getConnection(){
 
        $database = null;

        // Credentials of DB
        $user = "root";
        $pwd = "";
        try {
            $database = new PDO("mysql:host=localhost;dbname=archi_n_tiers;charset=utf8", $user, $pwd, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        } catch (PDOException $e) {
            die('Error connecting to mysql: ' . $e->getMessage()) or die(print_r($database->errorInfo()));
            exit();
        }
        return $database;
    }
}