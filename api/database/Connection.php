<?php

require_once('DatabaseManager.php');

class Connection extends DatabaseManager {
 
    // database connection and table name
    private $database;
    private $tableName = "connections";

    // constructor
    public function __construct(){
        $db = new DataBaseManager();
        $this->database = $db->getConnection();
    }
 
    // update a user record
    public function storeNewAuthentification($userId, $token, $expirationTime){
        $statement = $this->database->prepare("INSERT INTO $this->tableName(user_id, token, time_expiration) VALUES(:user_id, :token, :time_expiration)");
        $statement->bindParam(':user_id', $userId);
        $statement->bindParam(':token', $token);
        $statement->bindParam(':time_expiration', $expirationTime);

        if ($statement->execute()) {
            return true;
        }
        return false;
    }
    
}