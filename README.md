<h1>Architecture N Tiers</h1>
<h3>Audemard Lucas - Solane Alexandre</h3>

<p>Le projet a été réalisé en code natif via une architecture MVCT et à l'aide des langages PHP, MySQL, JavaScript, HTML et CSS.</p>
<p>Le projet est responsive selon différents viewports via Media Queries.</p>
<p>Serveur distant (actuellement en attente de confirmation d'autorisation) : </p>

<p>Vous trouvez dans le .zip envoyé par mail :</p>
<ul>
<li>la justification des technos</li>
<li>le screencast des fonctionnalités</li>
<li>le lien vers le repository gitlab</li>
</ul>

<p>Par ailleurs le code source est disponible sur la branche master (SQL compris)</p>

<p>Admin credentials : admin@tse-voyages.fr / password<p>


<p>Le responsive est limité par la faible utilisation de media queries</p>
